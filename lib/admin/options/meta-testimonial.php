<?php

$meta_boxes = array(
	'title' => sprintf( __( 'Testimonial Details', 'backstop-themes-admin' ), THEME_NAME ),
	'id' => 'mysite_testimonial_meta_box',
	'pages' => array( 'testimonial' ),
	'callback' => '',
	'context' => 'normal',
	'priority' => 'high',
	'fields' => array(
		array(
			'name' => __( 'Image', 'backstop-themes-admin' ),
			'desc' => __( 'Select the type of image you\'d liked displayed with your testimonial.', 'backstop-themes-admin' ),
			'id' => '_image',
			'options' => array( 
				'use_gravatar' => __( 'Use Gravatar', 'backstop-themes-admin' ),
				'upload_picture' => __( 'Upload Picture', 'backstop-themes-admin' ),
				'no_image' => __( 'No Image', 'backstop-themes-admin' )
			),
			'toggle' => 'toggle_true',
			'type' => 'radio'
		),
		array(
			'name' => __( 'Custom Image', 'backstop-themes-admin' ),
			'desc' => __( 'Upload an image to use for this testimonial.', 'backstop-themes-admin' ),
			'id' => '_custom_image',
			'toggle_class' => '_image_upload_picture',
			'type' => 'upload'
		),
		array(
			'name' => __( 'Email <small>(for Gravatar support)</small>', 'backstop-themes' ),
			'desc' => __( 'Enter the email address for the Gravatar you\'d like displayed, if no Gravatar is found your themes default will be used.', 'backstop-themes-admin' ),
			'id' => '_email',
			'toggle_class' => '_image_use_gravatar',
			'type' => 'text'
		),
		array(
			'name' => __( 'Name', 'backstop-themes' ),
			'desc' => __( 'Enter the name for this testimonial.', 'backstop-themes-admin' ),
			'id' => '_name',
			'type' => 'text'
		),
		array(
			'name' => __( 'Website Name', 'backstop-themes' ),
			'desc' => __( 'Enter the website name for this testimonial.', 'backstop-themes-admin' ),
			'id' => '_website_name',
			'type' => 'text'
		),
		array(
			'name' => __( 'Website URL', 'backstop-themes' ),
			'desc' => __( 'Enter the website url for this testimonial.', 'backstop-themes-admin' ),
			'id' => '_website_url',
			'type' => 'text'
		),
		array(
			'name' => __( 'Testimonial', 'backstop-themes-admin' ),
			'desc' => __( 'Enter your testimonial.', 'backstop-themes-admin' ),
			'id' => '_testimonial',
			'no_header' => true,
			'type' => 'editor'
		),
	)
);
return array(
	'load' => true,
	'options' => $meta_boxes
);

?>