<?php
/**
 *
 */
class mysiteMaps {
	
	private static $map_id = 1;
	
	/**
	 *
	 */
	function _map_id() {
	    return self::$map_id++;
	}

	/**
	 *
	 */
	function map( $atts = null, $content = null ) {
		if( $atts == 'generator' ) {
			$option = array(
				'name' => __( 'Maps', 'backstop-themes-admin' ),
				'value' => 'map',
				'options' => array(
					array(
						'name' => __( 'Width', 'backstop-themes-admin' ),
						'desc' => __( 'Type out the width of your map.', 'backstop-themes-admin' ),
						'id' => 'width',
						'default' => '',
						'type' => 'text',
						'shortcode_dont_multiply' => true
					),
					array(
						'name' => __( 'Height', 'backstop-themes-admin' ),
						'desc' => __( 'Type out the height of your map.', 'backstop-themes-admin' ),
						'id' => 'height',
						'default' => '',
						'type' => 'text',
						'shortcode_dont_multiply' => true
					),
					array(
						'name' => __( 'Zoom', 'backstop-themes-admin' ),
						'desc' => __( 'Select an initial zoom value for your map.', 'backstop-themes-admin' ),
						'id' => 'zoom',
						'options' => array(
							'1' => __('1', 'backstop-themes-admin' ),
							'2' => __('2', 'backstop-themes-admin' ),
							'3' => __('3', 'backstop-themes-admin' ),
							'4' => __('4', 'backstop-themes-admin' ),
							'5' => __('5', 'backstop-themes-admin' ),
							'6' => __('6', 'backstop-themes-admin' ),
							'7' => __('7', 'backstop-themes-admin' ),
							'8' => __('8', 'backstop-themes-admin' ),
							'9' => __('9', 'backstop-themes-admin' ),
							'10' => __('10', 'backstop-themes-admin' ),
							'11' => __('11', 'backstop-themes-admin' ),
							'12' => __('12', 'backstop-themes-admin' ),
							'13' => __('13', 'backstop-themes-admin' ),
							'14' => __('14', 'backstop-themes-admin' ),
							'15' => __('15', 'backstop-themes-admin' ),
						),
						'type' => 'select',
						'shortcode_dont_multiply' => true
					),
					array(
						'name' => __( 'Map Type', 'backstop-themes-admin' ),
						'desc' => __( 'Select which type of map you would like to use.', 'backstop-themes-admin' ),
						'id' => 'type',
						'default' => '',
						'options' => array(
							'ROADMAP' => __('Roadmap', 'backstop-themes-admin' ),
							'SATELLITE' => __('Satellite', 'backstop-themes-admin' ),
							'HYBRID' => __('Hybrid', 'backstop-themes-admin' ),
							'TERRAIN' => __('Terrain', 'backstop-themes-admin' ),
						),
						'type' => 'select',
						'shortcode_dont_multiply' => true
					),
					array(
						'name' => __( 'Number of Markers', 'backstop-themes-admin' ),
						"desc" => __( 'Select how many markers you wish to display on your map.', 'backstop-themes-admin' ),
						'id' => 'multiply',
						'options' => range(1,20),
						'type' => 'select',
						'shortcode_multiplier' => true
					),
					array(
						'name' => __( 'Address', 'backstop-themes-admin' ),
						'desc' => __( 'Type out the address for your marker.', 'backstop-themes-admin' ),
						'id' => 'address',
						'default' => '',
						'type' => 'text',
						'shortcode_multiply' => true
					),
					array(
						'name' => __( 'Description', 'backstop-themes-admin' ),
						'desc' => __( 'Type out the information you would like to display when your marker is clicked on.', 'backstop-themes-admin' ),
						'id' => 'content',
						'default' => '',
						'type' => 'text',
						'shortcode_multiply' => true
					),
					array(
						'name' => __( 'Icon', 'backstop-themes-admin' ),
						'desc' => __( 'You can upload an icon that you wish to use here.', 'backstop-themes-admin' ),
						'id' => 'icon',
						'default' => '',
						'type' => 'upload',
						'shortcode_multiply' => true
					),
					array(
						'value' => 'marker',
						'nested' => true
					),
				'shortcode_has_atts' => true,
				)
			);

			return $option;
		}
		
		global $wp_query, $mysite;
		
		extract(shortcode_atts(array(
			'width'			=> '400',
			'height'		=> '300',
			'zoom'			=> '4',
			'type'			=> 'ROADMAP',
		), $atts));
		
		$map_id = 'gmap_id_' . self::_map_id();
		
		// Load google maps api 
		$out = '<script type="text/javascript" src="maps.googleapis.com/maps/api/js?sensor=false"></script>';
		
		$out .= '<script type = "text/javascript">';
		$out .= 'jQuery(document).ready(function(){';
		
		// Setup options
		$out .= 'var options'.$map_id.' = {';
		$out .= 'zoom: '.$zoom.',';
		$out .= 'controls: [],';
		$out .= 'mapTypeId: google.maps.MapTypeId.'.$type;
		$out .= '};';
		
		// Initialize map
		$out .= 'var map'.$map_id.' = new google.maps.Map(document.getElementById("'.$map_id.'"), options'.$map_id.');';
		
		if ( !preg_match_all( '/(.?)\[(marker)\b(.*?)(?:(\/))?\](?:(.+?)\[\/marker\])?(.?)/s', $content, $matches ) ) {
	
			// No markers, do nothing

		} else {
		
			for ($i = 0; $i < count( $matches[0] ); $i++ ) {
			
				$options = explode('"', $matches[0][$i]);
				$address = $options[1];
				$icon = $options[3];
			
				$search_string = $matches[0][$i];
				$url_search = str_replace('[/marker]', '', $search_string);
				$info_content = substr($url_search, strpos($url_search, ']') + 1, strlen($url_search));
				$info_content = trim($info_content);
		
				// Setup a new Geocode for the current marker address
				$out .= 'var address'.$i.' = "";';
				$out .= 'var g'.$i.' = new google.maps.Geocoder();';
				$out .= 'g'.$i.'.geocode({ "address" : "'.$address.'" }, function (results, status) {';
					$out .= 'if (status == google.maps.GeocoderStatus.OK) {';
						$out .= 'address'.$i.' = results[0].geometry.location;';
						
						// Center map on last marker added
						$out .= 'map'.$map_id.'.setCenter(results[0].geometry.location);';
						
						// Setup Marker
						$out .= 'var marker'.$i.' = new google.maps.Marker({';
						$out .= 'position: address'.$i.','; 
						$out .= 'map: map'.$map_id.',';
						$out .= 'clickable: true,';
						$out .= 'icon: "'.$icon.'",';
						$out .= '});'; 
						
						// Setup info window for marker
						$out .= 'var infowindow'.$i.' = new google.maps.InfoWindow({ content: "'.$info_content.'" });';
						$out .= 'google.maps.event.addListener(marker'.$i.', "click", function() {';
						$out .= 'infowindow'.$i.'.open(map'.$map_id.', marker'.$i.');';
						$out .= '});';
						
					$out .= '}';
				$out .= '});';
				
			}
		}
		
		$out .= '});';
		$out .= '</script>';
		
		// Output our map container
		$out .= '<div id="'.$map_id.'" class = "msmw_map" style = "width: '.$width.'px; height: '.$height.'px;"></div>';
		
		return $out;
	}


	/**
	 *
	 */
	function _options( $class ) {
		$shortcode = array();
		
		$class_methods = get_class_methods( $class );
		
		foreach( $class_methods as $method ) {
			if( $method[0] != '_' )
				$shortcode[] = call_user_func(array( &$class, $method ), $atts = 'generator' );
		}
		
		$options = array(
			'name' => __( 'Maps', 'backstop-themes-admin' ),
			'value' => 'map',
			'options' => $shortcode
		);
		
		return $options;
	}
	
}

?>