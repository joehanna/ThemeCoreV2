<?php
/**
 * The BackStopThemes class. Defines the necessary constants 
 * and includes the necessary files for theme's operation.
 *
 * @package BackStopThemesCommon
 */

class BackStopThemesCommon {
	/**
	 * Initializes the theme framework by loading
	 * required files and functions for the theme.
	 *
	 * @since 1.0
	 */
	function init( $options ) {
		$this->load_theme_constants( $options );
		$this->load_theme_specific_constants( $options );
		$this->load_theme_functions();
		$this->load_theme_extensions();
		$this->load_theme_classes();
		$this->load_theme_variables();
		$this->load_theme_actions();
		$this->load_theme_filters();
		$this->load_theme_supports();
		$this->load_theme_locale();
		$this->load_theme_admin();
		$this->update_theme_status();
	}

	/**
	 * Define theme constants.
	 *
	 * @since 1.0
	 */
	private function load_theme_constants( $options ) {
		define( 'THEME_NAME', $options['theme_name'] );
		define( 'THEME_SLUG', get_template() );
		define( 'THEME_VERSION', $options['theme_version'] );
		define( 'FRAMEWORK_VERSION', '3.0' );
		define( 'DOCUMENTATION_URL', '//BackStopThemes.com/docs/index.php/Main_Page' );
		define( 'SUPPORT_URL', '//BackStopThemes.com/support' );
		define( 'MYSITE_PREFIX', 'mysite' );
		define( 'MYSITE_SETTINGS', 'mysite_' . THEME_SLUG . '_options' );
		define( 'MYSITE_INTERNAL_SETTINGS', 'mysite_' . THEME_SLUG . '_internal_options' );
		define( 'MYSITE_SIDEBARS', 'mysite_' . THEME_SLUG . '_sidebars' );
		define( 'MYSITE_SKINS', 'mysite_' . THEME_SLUG . '_skins' );
		define( 'MYSITE_ACTIVE_SKIN', 'mysite_' . THEME_SLUG . '_active_skin' );
		define( 'MYSITE_SKIN_NT_WRITABLE', 'mysite_' . THEME_SLUG . '_skins_nt_writable' );
		define( 'THEME_URI', get_template_directory_uri() );
		define( 'THEME_DIR', get_template_directory() );
		define( 'THEME_LIBRARY', THEME_DIR . '/lib' );
		define( 'THEME_ADMIN', THEME_LIBRARY . '/admin' );
		define( 'THEME_FUNCTIONS', THEME_LIBRARY . '/functions' );
		define( 'THEME_CLASSES', THEME_LIBRARY . '/classes' );
		define( 'THEME_EXTENSIONS', THEME_LIBRARY . '/extensions' );
		define( 'THEME_SHORTCODES', THEME_LIBRARY . '/shortcodes' );
		define( 'THEME_FONTS', THEME_LIBRARY . '/scripts/fonts' );
		define( 'THEME_STYLES_DIR', THEME_DIR . '/styles' );
		define( 'THEME_PATTERNS_DIR', THEME_STYLES_DIR . '/_patterns' );
		define( 'THEME_SPRITES_DIR', THEME_STYLES_DIR . '/_sprites' );
		define( 'THEME_IMAGES_DIR', THEME_DIR . '/images' );
		define( 'THEME_PATTERNS', '_patterns' );
		define( 'THEME_IMAGES', THEME_URI . '/images' );
		define( 'THEME_IMAGES_ASSETS', THEME_IMAGES . '/assets' );
		define( 'THEME_JS', THEME_URI . '/lib/scripts' );
		define( 'THEME_STYLES', THEME_URI . '/styles' );
		define( 'THEME_SPRITES', THEME_STYLES . '/_sprites' );
		define( 'THEME_ADMIN_FUNCTIONS', THEME_ADMIN . '/functions' );
		define( 'THEME_ADMIN_CLASSES', THEME_ADMIN . '/classes');
		define( 'THEME_ADMIN_OPTIONS', THEME_ADMIN . '/options');
		define( 'THEME_ADMIN_ASSETS_URI', THEME_URI . '/lib/admin/assets' );
		define( 'DEFAULT_SKIN', 'default.css' );
	}

	/**
	 * Loads theme functions.
	 *
	 * @since 1.0
	 */
	private function load_theme_functions() {
		require_once( THEME_DIR . '/activation.php' );
		require_once( THEME_FUNCTIONS . '/hooks-actions.php' );
		require_once( THEME_FUNCTIONS . '/context.php' );
		require_once( THEME_FUNCTIONS . '/core.php' );
		require_once( THEME_FUNCTIONS . '/theme.php' );
		require_once( THEME_FUNCTIONS . '/sliders.php' );
		require_once( THEME_FUNCTIONS . '/scripts.php' );
		require_once( THEME_FUNCTIONS . '/image.php' );
		require_once( THEME_FUNCTIONS . '/bookmarks.php' );
		require_once( THEME_FUNCTIONS . '/hooks-actions.php' );
		require_once( THEME_FUNCTIONS . '/compatibility.php' );
	}
	
	/**
	 * Loads theme extensions.
	 *
	 * @since 1.0
	 */
	private function load_theme_extensions() {
		require_once( THEME_EXTENSIONS . '/breadcrumbs-plus/breadcrumbs-plus.php' );
	}
	
	/**
	 * Loads theme classes.
	 *
	 * @since 1.0
	 */
	private function load_theme_classes() {
		require_once( THEME_CLASSES . '/twitter-api.php' );
		require_once( THEME_CLASSES . '/contact.php' );
		require_once( THEME_CLASSES . '/menu-walker.php' );
		require_once( THEME_CLASSES . '/raw-shortcode.php' );
	}

	
	/**
	 * Loads theme supports.
	 *
	 * @since 1.0
	 */
	private function load_theme_supports() {
		add_theme_support( 'load_theme_admin_menus' );
		add_theme_support( 'widgets' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'automatic-feed-links' );
	}
	
	/**
	 * Handles the locale functions file and translations.
	 *
	 * @since 1.0
	 */
	private function load_theme_locale() {
		# Get the user's locale.
		$locale = get_locale();

		if( is_admin() ) {
			# Load admin theme textdomain.
			load_theme_textdomain( 'backstop-themes-admin', THEME_ADMIN . '/languages' );
			$locale_file = THEME_ADMIN . "/languages/$locale.php";

		} else {
			# Load theme textdomain.
			load_theme_textdomain( 'backstop-themes', THEME_DIR . '/languages' );
			$locale_file = THEME_DIR . "/languages/$locale.php";
		}

		if ( is_readable( $locale_file ) )
			require_once( $locale_file );
	}
		
	/**
	 * Loads admin files.
	 *
	 * @since 1.0
	 */
	private function load_theme_admin() {
		if( !is_admin() ) return;
			
		require_once( THEME_ADMIN . '/admin.php' );
		(new mysiteAdmin)->init();
	}

	/**
	* This function updates the status presentation of non-WordPress.org themes on the dashboard, update-core.php,
	* and themes.php admin pages by changing the theme-specific update details in the WordPress update_themes transient
	* when these pages are loaded.
	*
	* DESIGN NOTE1: Use $_SERVER rather than $pagenow since the latter is set to index.php for the dashboard and all
	* posts & pages when logged in as admin (not sure why...seems like a core bug) and we do not want to run the
	* update check process on ALL the pages & posts accessed while logged in as admin.
	* 
	* DESIGN NOTE2: Do not use a callback to the set_site_transient_update_themes filter since it is called ONE OR MORE
	* times on EVERY admin page load.
	*
	* DESIGN NOTE2: Pressing the "Check Again" button will trigger a full theme update check cycle.
	*
	* (c) Copyright Tim Hibberd - OnePressTech Pty Ltd
	*/
	private function update_theme_status()
	{
		$current_uri = explode("?",$_SERVER['REQUEST_URI']);
		$current_slug = basename($current_uri[0]);
		if (current_user_can('update_themes') && in_array($current_slug , ['update-core.php', 'themes.php', 'index.php', 'network'])) {
			$wp_update_themes_transient = get_site_transient( 'update_themes' );
			$check_again_button_pressed = strpos($_SERVER['REQUEST_URI'],'update-core.php?force-check=1');
			$this->check_themes_are_uptodate($wp_update_themes_transient, $check_again_button_pressed);
			set_site_transient( 'update_themes', $wp_update_themes_transient );
		}
	}

	/**
	* This function retrieves a JSON file containing a list of the current theme versions. If any of
	* themes are newer than the current installed theme versions, this information is returned.
	*
	* DESIGN NOTE1: All theme update status changes are flagged to WordPress at the same time. There is no
	* operational or efficiency gain in updating them singly.
	*
	* DESIGN NOTE2: The JSON file will be cached for 12 hours unless overridden by a "Check Again" buttonpress.
	*
	* DESIGN NOTE3: No need for extensive error-checking & recovery since innacuracy is non-impacting.
	* The design is also self-healing through the use of the "Check Again" button. Impact of the transient not
	* persisting for the specified time simply translates into additional low cost, low latency JSON file retrievals.
	*
	* (c) Copyright Tim Hibberd - OnePressTech Pty Ltd
	*/
	private function check_themes_are_uptodate($wp_update_themes_transient, $check_again_button_pressed)
	{
		$theme_versions_file_data = get_site_transient('bst_theme_versions_file_data');
		if (empty($theme_versions_file_data) || $check_again_button_pressed) {
			$theme_versions_file = wp_remote_get('https://s3-ap-southeast-2.amazonaws.com/backstopthemes/theme-versions.json');
			if (wp_remote_retrieve_response_code($theme_versions_file) !== 200) {
				return;
			}
			$theme_versions_file_data = wp_remote_retrieve_body($theme_versions_file);
			set_site_transient( 'bst_theme_versions_file_data', $theme_versions_file_data, 12 * HOUR_IN_SECONDS );
		}

		$theme_versions_file_json = json_decode( $theme_versions_file_data, true );
		foreach ( $wp_update_themes_transient->checked as $theme => $installed_version) {
			if (!empty($theme_versions_file_json[$theme])) {
				$current_version = $theme_versions_file_json[$theme]['new_version'];
				if ( version_compare( $installed_version, $current_version, '<' )) {
					$wp_update_themes_transient->response[$theme] = (array) $theme_versions_file_json[$theme];
				}
			}
		}
		$wp_update_themes_transient->last_checked = time();
	}

}
?>