# V2.8.6 (24 Oct 2017)
- Housekeeping: Remove MSIE9 specific respond.min.js code since this browser is no longer supported
- Housekeeping: Add "alt" tags to html img statements that did not have one
- Housekeeping: Clean-up some comments and rename framework.php & framework-common.php to themecore.php and themecore-library.php
- Housekeeping: Delete local jQuery-form Javascript library and switch to using WordPress included version (upgrade from v2.49 --> v3.37.0)
- Housekeeping: Upgrade local PrettyPhoto Javascript library from v3.1.5 --> v3.1.6 and delete 1MB of unused test images
- Housekeeping: Upgrade local Galleria Javascript library from v1.2.7--> v1.5.7 and delete unused demo and theme files
- Bugfix: PrettyPhoto upgrade eliminates possible XSS vulnerability
- Enhancement: Altered theme auto-update logic to reset the transient timecheck after all themes have been checked rather than on each theme check
- Enhancement: Added post_slug class (examples: page_hello, post_welcome) to all posts except the front-page (which is a special case)
- Enhancement: Changed the post date format from a hardcoded format to the Administration > Settings > General date format
- Enhancement: Changed the post comments date format from a hardcoded format to the Administration > Settings > General date format
- NOTE: A software delta review indicates that the jQuery-form and PrettyPhoto upgrades should not affect existing software or CSS customisations
# V2.8.5 (3 Sep 2017)
- Housekeeping: Consolidate common init code across the 9 themes
- Housekeeping: Added php 5.4.0 check and warning message
- Housekeeping: Changed project name from framework to ThemeCore (WordPress.org does not accept frameworks)
- Bugfix: Fixed post comment bubble displaying as a text string
- Enhancement: Added auto theme version check and one-press update button (like WordPress.org theme update auto-check)
# V2.8.4 (28 Jul 2017)
- Housekeeping: Cleaned up CSS and php files to eliminate more ThemeCheck warnings & errors
- Housekeeping: Removed TimThumb - no longer supported by author, some hosting companies, WordPress Theme review team (NOTE: I left the user interface setting in place for expediency...it will be removed in the next release)
- Bugfix: Fixed Skin Manager not saving issue (missed a preg_replace modification required for correct php7 operation in last release)
- Bugfix: Fixed a shortcode issue in php7
- Bugfix: Fix Cufon BOLD L Character Flaw on Chrome browsers
# V2.8.3 (18 Jun 2017)
- Housekeeping: Increase theme version number from intended 1.0.3 to 2.8.3 to eliminate false WordPress security company warnings
- Housekeeping: Merged all theme-specific project files into the FrameworkV1.0 project for remote Git cloning convenience
- Housekeeping: IE8-IE9 support dropped from theme since they are obsolete and to eliminate browser debug warnings
- Housekeeping: Adjusted files to eliminate http references to eliminate mixed content warnings on SSL protected installations
- Housekeeping: Cleaned up CSS files to eliminate warnings related to deprecated CSS naming conventions
- Housekeeping: Cleaned up CSS and php files to eliminate more ThemeCheck warnings & errors
- Bugfix: Fixed Skin Manager not saving issue (preg_replace php function was changed by php gurus for security reasons and it broke skin mgr)
# V1.0.2 (5 Dec 2016)
- Bugfix: Rename themes to facilitate a smooth upgrade from MSMW themes to BSThemes. 
# V1.0.1 (5 Dec 2016)
- Bugfix: Remove function mysite_texturize_shortcode_before to work with Events-Manager plug-in (NOTE: Adding newline ahead of BST shortcodes now manual process)
- Bugfix: Fading slider was disappearing on home page
- Bugfix: Replace http:// with // or nothing so themes work with SSL and non-ssl implementations
- Bugfix: Correct WordPress warnings & errors up to and including WP4.6.1
- Bugfix: Correct PHP warnings & errors up to and including PHP7
- Enhancment: Add theme option metaboxes to custom posts
- Enhancment: Replaced gzencoded compressed default options with expanded options for traceability and to pass theme security checks
# V1.0.0 (19 Nov 2016)
- Check-in the fork of the legacy GPL MySiteMyWay Construct theme skin frame (Contruct theme specific files).
