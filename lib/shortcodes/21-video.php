<?php
/**
 *
 */
class mysiteVideo {
	
	private static $video_id = 1;
	
	/**
	 *
	 */
	function _video_id() {
	    return self::$video_id++;
	}
	
	/**
	 * http://code.google.com/intl/en/apis/youtube/player_parameters.html
	 */
	function youtube( $atts = null, $content = null ) {
		if( $atts == 'generator' ) {
			$option = array(
				'name' => __( 'YouTube', 'backstop-themes-admin' ),
				'value' => 'youtube',
				'options' => array(
					array(
						'name' => __( 'YouTube Video URL', 'backstop-themes-admin' ),
						'desc' => __( 'When viewing your youtube video in your web browser copy the URL and paste it here.  Here is an example of what it might look like,<br /><br />http://www.youtube.com/watch?v=SX728EemOPOIw&feature=related', 'backstop-themes-admin' ),
						'id' => 'url',
						'default' => '',
						'type' => 'text'
					),
					array(
						'name' => __( 'Width', 'backstop-themes-admin' ),
						'desc' => __( 'You can set the width of your video here.', 'backstop-themes-admin' ),
						'id' => 'width',
						'default' => '',
						'type' => 'text'
					),
					array(
						'name' => __( 'Height', 'backstop-themes-admin' ),
						'desc' => __( 'You can set the height of your video here.', 'backstop-themes-admin' ),
						'id' => 'height',
						'default' => '',
						'type' => 'text'
					),
					array(
						'name' => __( 'Autohide', 'backstop-themes-admin' ),
						'desc' => __( 'You can hide various youtube displays from showing on your video.', 'backstop-themes-admin' ),
						'id' => 'autohide',
						'options' => array(
							'0' => __( 'Visible', 'backstop-themes-admin' ),
							'1' => __( 'Hide all', 'backstop-themes-admin' ),
							'2' => __( 'Hide video progress bar', 'backstop-themes-admin' ),
						),
						'type' => 'select',
					),
					array(
						'name' => __( 'Autoplay', 'backstop-themes-admin' ),
						'desc' => __( 'Check this to have your video play after it is loaded.', 'backstop-themes-admin' ),
						'id' => 'autoplay',
						'options' => array( '1' => __( 'Video will autoplay when the player loads', 'backstop-themes-admin' ) ),
						'type' => 'checkbox'
					),
					array(
						'name' => __( 'Controls', 'backstop-themes-admin' ),
						'desc' => __( 'Check this to have the youtube player controls display.', 'backstop-themes-admin' ),
						'id' => 'controls',
						'options' => array( '0' => __( 'Disable video player controls', 'backstop-themes-admin' ) ),
						'type' => 'checkbox'
					),
					array(
						'name' => __( 'Disable KB', 'backstop-themes-admin' ),
						'desc' => __( 'Check this to disable the keyboard controls', 'backstop-themes-admin' ),
						'options' => array( '1' => __( 'Disable the player keyboard controls', 'backstop-themes-admin' ) ),
						'id' => 'disablekb',
						'type' => 'checkbox'
					),
					array(
						'name' => __( 'Fullscreen', 'backstop-themes-admin' ),
						'desc' => __( 'Check this to allow your video to be played in fullscreen mode.<br /><br />A small button will display in the bottom right hand corner for fullscreen mode.', 'backstop-themes-admin' ),
						'options' => array( '1' => __( 'Enables the fullscreen button in the video player', 'backstop-themes-admin' ) ),
						'id' => 'fs',
						'type' => 'checkbox'
					),
					array(
						'name' => __( 'HD', 'backstop-themes-admin' ),
						'desc' => __( 'Checking this will enable your video to be viewed in HD format.', 'backstop-themes-admin' ),
						'options' => array( '1' => __( 'Enables HD video playback by default', 'backstop-themes-admin' ) ),
						'id' => 'hd',
						'type' => 'checkbox'
					),
					array(
						'name' => __( 'Loop', 'backstop-themes-admin' ),
						'desc' => __( 'This will set your video to loop.', 'backstop-themes-admin' ),
						'options' => array( '1' => __( 'Play the initial video again and again', 'backstop-themes-admin' ) ),
						'id' => 'loop',
						'type' => 'checkbox'
					),
					array(
						'name' => __( 'Related Videos', 'backstop-themes-admin' ),
						'desc' => __( 'Checking this will disable the related videos from appearing once your video is done playing.', 'backstop-themes-admin' ),
						'options' => array( '0' => __( 'Disable related videos once playback of the initial video starts', 'backstop-themes-admin' ) ),
						'id' => 'rel',
						'type' => 'checkbox'
					),
					array(
						'name' => __( 'Search', 'backstop-themes-admin' ),
						'desc' => __( 'Checking this will disable the search box from displaying when the video is minimized.', 'backstop-themes-admin' ),
						'options' => array( '0' => __( 'Disables the search box from displaying when the video is minimized', 'backstop-themes-admin' ) ),
						'id' => 'showsearch',
						'type' => 'checkbox'
					),
					array(
						'name' => __( 'Info', 'backstop-themes-admin' ),
						'desc' => __( 'Checking this will disable the display information.', 'backstop-themes-admin' ),
						'options' => array( '0' => __( 'Disable display information', 'backstop-themes-admin' ) ),
						'id' => 'showinfo',
						'type' => 'checkbox'
					),
				'shortcode_has_atts' => true
				)
			);
			
			return $option;
		}
		
		extract(shortcode_atts(array(
			'url' 		=> false,
			'width' 	=> '',
			'height' 	=> '',
			'autohide'  => '2',
			'autoplay'  => '0',
			'controls'  => '1',
			'disablekb' => '0',
			'fs'        => '0',
			'hd'        => '0',
			'loop'      => '0',
			'rel'       => '1',
			'showsearch'=> '1',
			'showinfo'  => '1',
		), $atts));
		
		if( !$url )
			return __( 'Please enter the url to a YouTube video.', 'backstop-themes' );
			
		if ( preg_match( '/^https?\:\/\/(?:(?:[a-zA-Z0-9\-\_\.]+\.|)youtube\.com\/watch\?v\=|youtu\.be\/)([a-zA-Z0-9\-\_]+)/i', $url, $matches ) > 0 )
	        $video_id = $matches[1];
	
	    elseif ( preg_match('/^([a-zA-Z0-9\-\_]+)$/i', $url, $matches ) > 0 )
	        $video_id = $matches[1];
	
		if( !isset( $video_id ) )
			return __( 'There was an error retrieving the YouTube video ID for the url you entered, please verify that the url is correct.', 'backstop-themes' );
			
		$width = ( !empty( $width ) ) ? trim(str_replace(' ', '', str_replace('px', '', $width ) ) ) : '560';
		$height = ( !empty( $height ) ) ? trim(str_replace(' ', '', str_replace('px', '', $height ) ) ) : '340';
		
		$_video_id = self::_video_id();
				
		return "<div class='video_frame'><iframe id='youtube_video_$_video_id' class='youtube_video' style='height:{$height}px;width:{$width}px' src='//www.youtube.com/embed/{$video_id}?autohide={$autohide}&amp;autoplay={$autoplay}&amp;controls={$controls}&amp;disablekb={$disablekb}&amp;fs={$fs}&amp;hd={$hd}&amp;loop={$loop}&amp;rel={$rel}&amp;showinfo={$showinfo}&amp;showsearch={$showsearch}&amp;wmode=transparent&amp;enablejsapi=1' width='{$width}' height='{$height}' frameborder='0'></iframe></div>";
		
		
	}
	
	/**
	 * http://vimeo.com/api/docs/player
	 */
	function vimeo( $atts = null, $content = null ) {
		if( $atts == 'generator' ) {
			$option = array( 
				'name' => __( 'Vimeo', 'backstop-themes-admin' ),
				'value' => 'vimeo',
				'options' => array(
					array(
						'name' => __( 'Vimeo Video URL', 'backstop-themes-admin' ),
						'desc' => __( 'When viewing your vimeo video in your web browser copy the URL and paste it here.  Here is an example of what it might look like,<br /><br />http://vimeo.com/12345678', 'backstop-themes-admin' ),
						'id' => 'url',
						'default' => '',
						'type' => 'text'
					),
					array(
						'name' => __( 'Width', 'backstop-themes-admin' ),
						'desc' => __( 'You can set the width of your video here.', 'backstop-themes-admin' ),
						'id' => 'width',
						'default' => '',
						'type' => 'text'
					),
					array(
						'name' => __( 'Height', 'backstop-themes-admin' ),
						'desc' => __( 'You can set the height of your video here.', 'backstop-themes-admin' ),
						'id' => 'height',
						'default' => '',
						'type' => 'text'
					),
					array(
						'name' => __( 'Title', 'backstop-themes-admin' ),
						'desc' => __( 'Checking this will disable your title from displaying on the video.', 'backstop-themes-admin' ),
						'options' => array( '0' => __( 'Disable title on the video', 'backstop-themes-admin' ) ),
						'id' => 'title',
						'type' => 'checkbox'
					),
					array(
						'name' => __( 'Byline', 'backstop-themes-admin' ),
						'desc' => __( 'Checking this will disable your byline from displaying on the video.<br /><br />The byline appears right below your title.', 'backstop-themes-admin' ),
						'options' => array( '0' => __( 'Disable byline on the video', 'backstop-themes-admin' ) ),
						'id' => 'fs',
						'type' => 'checkbox'
					),
					array(
						'name' => __( 'Portrait', 'backstop-themes-admin' ),
						'desc' => __( 'Checking this will disable the portrait image from displaying on the video.<br /><br />The portrait image appears to the left of the title and byline.', 'backstop-themes-admin' ),
						'options' => array( '0' => __( "Disable user's portrait on the video", 'backstop-themes-admin' ) ),
						'id' => 'portrait',
						'type' => 'checkbox'
					),
					array(
						'name' => __( 'Autoplay', 'backstop-themes-admin' ),
						'desc' => __( 'Check to have your video play automatically after it is loaded.', 'backstop-themes-admin' ),
						'options' => array( '1' => __( 'Play the video automatically on load', 'backstop-themes-admin' ) ),
						'id' => 'autoplay',
						'type' => 'checkbox'
					),
					array(
						'name' => __( 'Loop', 'backstop-themes-admin' ),
						'desc' => __( 'Check to have your video loop after it is done playing.', 'backstop-themes-admin' ),
						'options' => array( '1' => __( 'Play the video again when it reaches the end', 'backstop-themes-admin' ) ),
						'id' => 'loop',
						'type' => 'checkbox'
					),
				'shortcode_has_atts' => true
				)
			);
			
			return $option;
		}
		
		extract(shortcode_atts(array(
			'url' 		=> false,
			'width' 	=> '',
			'height' 	=> '',
			'title'     => '1',
			'byline'    => '1',
			'portrait'  => '1',
			'autoplay'  => '0',
			'loop'      => '0',
		), $atts));
		
		
		if( !$url )
			return __( 'Please enter the url to a Vimeo video.', 'backstop-themes' );
			
		if ( preg_match( '#https?://(www.vimeo|vimeo)\.com(/|/clip:)(\d+)(.*?)#i', $url, $matches ) > 0 )
	        $video_id = $matches[3];
	
	    elseif ( preg_match('/^([a-zA-Z0-9\-\_]+)$/i', $url, $matches ) > 0 )
	        $video_id = $matches[1];
	
		if( !isset( $video_id ) )
			return __( 'There was an error retrieving the Vimeo video ID for the url you entered, please verify that the url is correct.', 'backstop-themes' );
			
		$width = ( !empty( $width ) ) ? trim(str_replace(' ', '', str_replace('px', '', $width ) ) ) : '560';
		$height = ( !empty( $height ) ) ? trim(str_replace(' ', '', str_replace('px', '', $height ) ) ) : '340';
		
		$_video_id = self::_video_id();
		
		return "<div class='video_frame'><iframe id='vimeo_video_$_video_id' class='vimeo_video' style='height:{$height}px;width:{$width}px' src='//player.vimeo.com/video/{$video_id}?title={$title}&amp;byline={$byline}&amp;portrait={$portrait}&amp;autoplay={$autoplay}&amp;loop={$loop}&amp;js_api=1&amp;js_swf_id=vimeo_video_$_video_id' width='{$width}' height='{$height}' frameborder='0'></iframe></div>";
		
	}
		
	/**
	 *
	 */
	function _options( $class ) {
		$shortcode = array();
		
		$class_methods = get_class_methods($class);

		foreach( $class_methods as $method ) {
			if( $method[0] != '_' )
				$shortcode[] = call_user_func(array( &$class, $method ), $atts = 'generator' );
		}

		$options = array(
			'name' => __( 'Video', 'backstop-themes-admin' ),
			'desc' => __( 'Choose which type of video you wish to use.', 'backstop-themes-admin' ),
			'value' => 'video',
			'options' => $shortcode,
			'shortcode_has_types' => true
		);

		return $options;
	}

}

?>