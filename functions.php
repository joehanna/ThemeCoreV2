<?php
/**
 * Sets up the theme by loading the BackStopThemes class & triggers the init function
 * which activates all classes and functions needed for the theme's operation.
 *
 * @package BackStopThemes
 * @subpackage Functions
 */

// Check for minimum required PHP version.
define('THEME_REQUIRED_PHP_VERSION', '5.4.0');
if ( version_compare(phpversion(), THEME_REQUIRED_PHP_VERSION, '<') ) {
	add_action( 'admin_notices', 'my_admin_notice' );
	function my_admin_notice() {
	?>
		<div class="notice notice-error is-dismissible">
		<p><?php _e( 'BackStop Themes require that your PHP version be upgraded from your current version ', 'backstop-themes' ) ?> <strong><?php echo phpversion(); ?></strong> <?php _e( ' to at least version ', 'backstop-themes' ) ?> <strong><?php echo THEME_REQUIRED_PHP_VERSION; ?></strong></p>
		</div>
	<?php
	}
}

# disable the WordPress post look-ahead setting...it just thrashes the server unnecessarily
remove_filter('wp_head','adjacent_posts_rel_link_wp_head',10);

# Load the BackStopThemes class.
require_once( get_template_directory() . '/themecore-library.php' );
require_once( get_template_directory() . '/themecore.php' );

# Get theme data and Initialize the theme.
$theme_data = wp_get_theme();
(new BackStopThemes)->init(array(
	'theme_name' => $theme_data->get( 'Name' ),
	'theme_uri' => $theme_data->get( 'ThemeURI' ),
	'theme_description' => $theme_data->get( 'Description' ),
	'theme_author' => $theme_data->get( 'Author' ),
	'theme_authoruri' => $theme_data->get( 'AuthorURI' ),
	'theme_version' => $theme_data->get( 'Version' ),
	'theme_text_domain' => $theme_data->get( 'TextDomain' )
));

?>