<?php
/**
 * The BackStopThemes class. Defines the necessary constants 
 * and includes the necessary files for theme's operation.
 *
 * @package BackStopThemes
 * @subpackage Fusion
 */

class BackStopThemes extends BackStopThemesCommon {
	
	public function load_theme_specific_constants( $options ) {
		define( 'FANCY_PORTFOLIO', TRUE );
	}
	
	/**
	 * Loads theme actions.
	 *
	 * @since 1.0
	 */
	public function load_theme_actions() {
		
		# WordPress actions
		add_action( 'init', 'mysite_is_mobile_device' );
		add_action( 'init', 'mysite_is_responsive' );
		add_action( 'init', 'mysite_shortcodes_init' );
		add_action( 'init', 'mysite_menus' );
		add_action( 'init', 'mysite_post_types'  );
		add_action( 'init', 'mysite_register_script' );
		add_action( 'init', 'mysite_wp_image_resize', 11 );
		add_action( 'init', array( 'mysiteForm', 'init'), 11 );
		add_action( 'widgets_init', 'mysite_sidebars' );
		add_action( 'widgets_init', 'mysite_widgets' );
		add_action( 'wp_head', 'mysite_seo_meta' );
		add_action( 'wp_head', 'mysite_mobile_meta' );
		add_action( 'wp_head', 'mysite_analytics' );
		add_action( 'wp_head', 'mysite_custom_bg' );
		add_action( 'wp_head', 'mysite_additional_headers', 99 );
		add_action( 'wp_head', 'mysite_fitvids' );
		add_action( 'template_redirect', 'mysite_enqueue_script' );
		add_action( 'template_redirect', 'mysite_squeeze_page' );
		add_action( 'comment_form_defaults', 'mysite_comment_form_args' );
		remove_action( 'wp_head', 'rel_canonical' );
		
		# BackStopThemes actions
		add_action( 'mysite_head', 'mysite_header_scripts' );
		add_action( 'mysite_before_header', 'mysite_fullscreen_bg' );
		add_action( 'mysite_header', 'mysite_logo' );
		add_action( "mysite_header", 'mysite_primary_menu' );
		add_action( 'mysite_header', 'mysite_responsive_menu' );
		add_action( 'mysite_after_header', 'mysite_header_extras' );
		add_action( 'mysite_after_header', 'mysite_slider_module' );
		add_action( 'mysite_after_header', 'mysite_teaser' );
		add_action( 'mysite_intro_end', 'mysite_post_meta_single' );
		add_action( 'mysite_before_post', 'mysite_post_image' );
		add_action( 'mysite_before_page_content', 'mysite_home_content' );
		add_action( 'mysite_before_page_content', 'mysite_page_content' );
		add_action( 'mysite_before_page_content', 'mysite_page_title' );
		add_action( 'mysite_before_page_content', 'mysite_query_posts' );
		add_action( 'mysite_portfolio_image_end', 'mysite_portfolio_meta' );
		add_action( 'mysite_post_image_end', 'mysite_fancy_read_more' );
		add_action( 'mysite_post_image_end', 'mysite_post_img_overlay' );
		add_action( 'mysite_portfolio_image_end', 'mysite_post_img_overlay' );
		add_action( 'mysite_singular-post_post_image_end', 'mysite_post_sociables' );
		add_action( 'mysite_after_portfolio_image', 'mysite_post_img_overlay' );
		add_action( 'mysite_singular-page_before_entry', 'mysite_post_image' );
		add_action( 'mysite_singular-page_before_entry', 'mysite_breadcrumbs' );
		add_action( 'mysite_singular-portfolio_before_entry', 'mysite_breadcrumbs' );
		add_action( 'mysite_singular-post_before_entry', 'mysite_breadcrumbs' );
		add_action( 'mysite_before_entry', 'mysite_post_meta' );
		add_action( 'mysite_blog_before_entry', 'mysite_post_title' );
		add_action( 'mysite_archive_before_entry', 'mysite_post_title' );
		add_action( 'mysite_singular-portfolio_before_entry', 'mysite_portfolio_date' );
		add_action( 'mysite_singular-portfolio_before_entry', 'mysite_post_title' );
		add_action( 'mysite_singular-post_after_entry', 'mysite_post_nav' );
		add_action( 'mysite_singular-post_after_entry', 'mysite_post_meta_bottom' );
		add_action( 'mysite_singular-post_after_post', 'mysite_like_module' );
		add_action( 'mysite_singular-post_after_post', 'mysite_about_author' );
		add_action( 'mysite_after_post', 'mysite_page_navi' );
		add_action( 'mysite_after_main', 'mysite_get_sidebar' );
		add_action( 'mysite_before_footer', 'mysite_footer_teaser' );
		add_action( 'mysite_footer', 'mysite_main_footer' );
		add_action( 'wp_footer', 'mysite_sub_footer' );
		add_action( 'mysite_body_end', 'mysite_print_cufon' );
		add_action( 'mysite_body_end', 'mysite_image_preloading' );
		add_action( 'mysite_body_end', 'mysite_intro_javascript' );
		add_action( 'mysite_body_end', 'mysite_ios_rotate' );
		add_action( 'mysite_body_end', 'mysite_custom_javascript' );
	}
	
	/**
	 * Loads theme filters.
	 *
	 * @since 1.0
	 */
	public function load_theme_filters() {
		
		# BackStopThemes filters
		add_filter( 'mysite_teaser_single_title', 'mysite_teaser_single_title' );
		add_filter( 'mysite_teaser', 'mysite_entry_title' );
		add_filter( 'mysite_read_more', create_function('','return;') );
		add_filter( 'mysite_portfolio_date_top', 'mysite_portfolio_date_top', 1, 2 );
		add_filter( 'mysite_portfolio_date', create_function('','return;') );
		add_filter( 'mysite_portfolio_read_more', create_function('','return;') );
		add_filter( 'mysite_portfolio_visit_site', create_function('','return;') );
		add_filter( 'mysite_fancy_link', 'mysite_fancy_link', 1, 2 );
		add_filter( 'mysite_additional_posts_title', create_function('','return;') );
		add_filter( 'the_content_more_link', 'mysite_full_read_more', 10, 2 );
		add_filter( 'excerpt_length', 'mysite_excerpt_length_long', 999 );
		add_filter( 'excerpt_more', 'mysite_excerpt_more' );
		add_filter( 'posts_where', 'mysite_multi_tax_terms' );
		add_filter( 'pre_get_posts', 'mysite_exclude_category_feed' );
		add_filter( 'pre_get_posts', 'mysite_custom_search' );
		add_filter( 'widget_categories_args', 'mysite_exclude_category_widget' );
		add_filter( 'query_vars', 'mysite_queryvars' );
		add_filter( 'rewrite_rules_array', 'mysite_rewrite_rules',10,2 );
		add_filter( 'widget_text', 'do_shortcode' );
		add_filter( 'wp_page_menu_args', 'mysite_page_menu_args' );
		add_filter( 'the_password_form', 'mysite_password_form' );
	}

	/**
	 * Define theme variables.
	 *
	 * @since 1.0
	 */
	public function load_theme_variables() {
		global $mysite;
		
		$layout = '';
		$img_set = get_option( MYSITE_SETTINGS );
		$img_set = ( !empty( $img_set ) && !isset( $_POST[MYSITE_SETTINGS]['reset'] ) ) ? $img_set : array();
		$blog_layout = apply_filters( 'mysite_blog_layout', mysite_get_setting( 'blog_layout' ) );
		
		# Images
		$images = array(
			'one_column_portfolio' => array( 
				( !empty( $img_set['one_column_portfolio_full']['w'] ) ? $img_set['one_column_portfolio_full']['w'] : 920 ),
				( !empty( $img_set['one_column_portfolio_full']['h'] ) ? $img_set['one_column_portfolio_full']['h'] : 634 )),
			'two_column_portfolio' => array( 
				( !empty( $img_set['two_column_portfolio_full']['w'] ) ? $img_set['two_column_portfolio_full']['w'] : 441 ),
				( !empty( $img_set['two_column_portfolio_full']['h'] ) ? $img_set['two_column_portfolio_full']['h'] : 304 )),
			'three_column_portfolio' => array( 
				( !empty( $img_set['three_column_portfolio_full']['w'] ) ? $img_set['three_column_portfolio_full']['w'] : 282 ),
				( !empty( $img_set['three_column_portfolio_full']['h'] ) ? $img_set['three_column_portfolio_full']['h'] : 194 )),
			'four_column_portfolio' => array( 
				( !empty( $img_set['four_column_portfolio_full']['w'] ) ? $img_set['four_column_portfolio_full']['w'] : 202 ),
				( !empty( $img_set['four_column_portfolio_full']['h'] ) ? $img_set['four_column_portfolio_full']['h'] : 139 )),

			'one_column_blog' => array( 
				( !empty( $img_set['one_column_blog_full']['w'] ) ? $img_set['one_column_blog_full']['w'] : 920 ),
				( !empty( $img_set['one_column_blog_full']['h'] ) ? $img_set['one_column_blog_full']['h'] : 460 )),
			'two_column_blog' => array( 
				( !empty( $img_set['two_column_blog_full']['w'] ) ? $img_set['two_column_blog_full']['w'] : 441 ),
				( !empty( $img_set['two_column_blog_full']['h'] ) ? $img_set['two_column_blog_full']['h'] : 220 )),
			'three_column_blog' => array( 
				( !empty( $img_set['three_column_blog_full']['w'] ) ? $img_set['three_column_blog_full']['w'] : 282 ),
				( !empty( $img_set['three_column_blog_full']['h'] ) ? $img_set['three_column_blog_full']['h'] : 141 )),
			'four_column_blog' => array( 
				( !empty( $img_set['four_column_blog_full']['w'] ) ? $img_set['four_column_blog_full']['w'] : 202 ),
				( !empty( $img_set['four_column_blog_full']['h'] ) ? $img_set['four_column_blog_full']['h'] : 101 )),

		    'small_post_list' => array( 
		        ( !empty( $img_set['small_post_list_full']['w'] ) ? $img_set['small_post_list_full']['w'] : 80 ),
		        ( !empty( $img_set['small_post_list_full']['h'] ) ? $img_set['small_post_list_full']['h'] : 80 )),
		    'medium_post_list' => array( 
		        ( !empty( $img_set['medium_post_list_full']['w'] ) ? $img_set['medium_post_list_full']['w'] : 250 ),
		        ( !empty( $img_set['medium_post_list_full']['h'] ) ? $img_set['medium_post_list_full']['h'] : 285 )),
			'large_post_list' => array( 
				( !empty( $img_set['large_post_list_full']['w'] ) ? $img_set['large_post_list_full']['w'] : 601 ),
				( !empty( $img_set['large_post_list_full']['h'] ) ? $img_set['large_post_list_full']['h'] : 373 )),

		    'portfolio_single_full' => array( 
		        ( !empty( $img_set['portfolio_single_full_full']['w'] ) ? $img_set['portfolio_single_full_full']['w'] : 980 ),
		        ( !empty( $img_set['portfolio_single_full_full']['h'] ) ? $img_set['portfolio_single_full_full']['h'] : 675 )),
		    'additional_posts_grid' => array( 
		        ( !empty( $img_set['additional_posts_grid_full']['w'] ) ? $img_set['additional_posts_grid_full']['w'] : 237 ),
		        ( !empty( $img_set['additional_posts_grid_full']['h'] ) ? $img_set['additional_posts_grid_full']['h'] : 164 )),

		);

		$big_sidebar_images = array(
			'one_column_portfolio' => array( 
				( !empty( $img_set['one_column_portfolio_big']['w'] ) ? $img_set['one_column_portfolio_big']['w'] : 660 ),
				( !empty( $img_set['one_column_portfolio_big']['h'] ) ? $img_set['one_column_portfolio_big']['h'] : 455 )),
			'two_column_portfolio' => array( 
				( !empty( $img_set['two_column_portfolio_big']['w'] ) ? $img_set['two_column_portfolio_big']['w'] : 317 ),
				( !empty( $img_set['two_column_portfolio_big']['h'] ) ? $img_set['two_column_portfolio_big']['h'] : 217 )),
			'three_column_portfolio' => array( 
				( !empty( $img_set['three_column_portfolio_big']['w'] ) ? $img_set['three_column_portfolio_big']['w'] : 202 ),
				( !empty( $img_set['three_column_portfolio_big']['h'] ) ? $img_set['three_column_portfolio_big']['h'] : 139 )),
			'four_column_portfolio' => array( 
				( !empty( $img_set['four_column_portfolio_big']['w'] ) ? $img_set['four_column_portfolio_big']['w'] : 145 ),
				( !empty( $img_set['four_column_portfolio_big']['h'] ) ? $img_set['four_column_portfolio_big']['h'] : 100 )),

			'one_column_blog' => array( 
				( !empty( $img_set['one_column_blog_big']['w'] ) ? $img_set['one_column_blog_big']['w'] : 660 ),
				( !empty( $img_set['one_column_blog_big']['h'] ) ? $img_set['one_column_blog_big']['h'] : 330 )),
			'two_column_blog' => array( 
				( !empty( $img_set['two_column_blog_big']['w'] ) ? $img_set['two_column_blog_big']['w'] : 317 ),
				( !empty( $img_set['two_column_blog_big']['h'] ) ? $img_set['two_column_blog_big']['h'] : 158 )),
			'three_column_blog' => array( 
				( !empty( $img_set['three_column_blog_big']['w'] ) ? $img_set['three_column_blog_big']['w'] : 202 ),
				( !empty( $img_set['three_column_blog_big']['h'] ) ? $img_set['three_column_blog_big']['h'] : 101 )),
			'four_column_blog' => array( 
				( !empty( $img_set['four_column_blog_big']['w'] ) ? $img_set['four_column_blog_big']['w'] : 145 ),
				( !empty( $img_set['four_column_blog_big']['h'] ) ? $img_set['four_column_blog_big']['h'] : 72 )),

		    'small_post_list' => array( 
		        ( !empty( $img_set['small_post_list_big']['w'] ) ? $img_set['small_post_list_big']['w'] : 80 ),
		        ( !empty( $img_set['small_post_list_big']['h'] ) ? $img_set['small_post_list_big']['h'] : 80 )),
		    'medium_post_list' => array( 
		        ( !empty( $img_set['medium_post_list_big']['w'] ) ? $img_set['medium_post_list_big']['w'] : 250 ),
		        ( !empty( $img_set['medium_post_list_big']['h'] ) ? $img_set['medium_post_list_big']['h'] : 285 )),
			'large_post_list' => array( 
				( !empty( $img_set['large_post_list_big']['w'] ) ? $img_set['large_post_list_big']['w'] : 431 ),
				( !empty( $img_set['large_post_list_big']['h'] ) ? $img_set['large_post_list_big']['h'] : 267 )),

		    'portfolio_single_full' => array( 
		        ( !empty( $img_set['portfolio_single_full_big']['w'] ) ? $img_set['portfolio_single_full_big']['w'] : 660 ),
		        ( !empty( $img_set['portfolio_single_full_big']['h'] ) ? $img_set['portfolio_single_full_big']['h'] : 455 )),
		    'additional_posts_grid' => array( 
		        ( !empty( $img_set['additional_posts_grid_big']['w'] ) ? $img_set['additional_posts_grid_big']['w'] : 172 ),
		        ( !empty( $img_set['additional_posts_grid_big']['h'] ) ? $img_set['additional_posts_grid_big']['h'] : 118 )),

		);

		$small_sidebar_images = array(
			'one_column_portfolio' => array( 
				( !empty( $img_set['one_column_portfolio_small']['w'] ) ? $img_set['one_column_portfolio_small']['w'] : 690 ),
				( !empty( $img_set['one_column_portfolio_small']['h'] ) ? $img_set['one_column_portfolio_small']['h'] : 475 )),
			'two_column_portfolio' => array( 
				( !empty( $img_set['two_column_portfolio_small']['w'] ) ? $img_set['two_column_portfolio_small']['w'] : 331 ),
				( !empty( $img_set['two_column_portfolio_small']['h'] ) ? $img_set['two_column_portfolio_small']['h'] : 228 )),
			'three_column_portfolio' => array( 
				( !empty( $img_set['three_column_portfolio_small']['w'] ) ? $img_set['three_column_portfolio_small']['w'] : 211 ),
				( !empty( $img_set['three_column_portfolio_small']['h'] ) ? $img_set['three_column_portfolio_small']['h'] : 145 )),
			'four_column_portfolio' => array( 
				( !empty( $img_set['four_column_portfolio_small']['w'] ) ? $img_set['four_column_portfolio_small']['w'] : 151 ),
				( !empty( $img_set['four_column_portfolio_small']['h'] ) ? $img_set['four_column_portfolio_small']['h'] : 104 )),

			'one_column_blog' => array( 
				( !empty( $img_set['one_column_blog_small']['w'] ) ? $img_set['one_column_blog_small']['w'] : 690 ),
				( !empty( $img_set['one_column_blog_small']['h'] ) ? $img_set['one_column_blog_small']['h'] : 345 )),
			'two_column_blog' => array( 
				( !empty( $img_set['two_column_blog_small']['w'] ) ? $img_set['two_column_blog_small']['w'] : 331 ),
				( !empty( $img_set['two_column_blog_small']['h'] ) ? $img_set['two_column_blog_small']['h'] : 165 )),
			'three_column_blog' => array( 
				( !empty( $img_set['three_column_blog_small']['w'] ) ? $img_set['three_column_blog_small']['w'] : 211 ),
				( !empty( $img_set['three_column_blog_small']['h'] ) ? $img_set['three_column_blog_small']['h'] : 105 )),
			'four_column_blog' => array( 
				( !empty( $img_set['four_column_blog_small']['w'] ) ? $img_set['four_column_blog_small']['w'] : 151 ),
				( !empty( $img_set['four_column_blog_small']['h'] ) ? $img_set['four_column_blog_small']['h'] : 75 )),

			'small_post_list' => array( 
				( !empty( $img_set['small_post_list_small']['w'] ) ? $img_set['small_post_list_small']['w'] : 50 ),
				( !empty( $img_set['small_post_list_small']['h'] ) ? $img_set['small_post_list_small']['h'] : 50 )),
			'medium_post_list' => array( 
				( !empty( $img_set['medium_post_list_small']['w'] ) ? $img_set['medium_post_list_small']['w'] : 250 ),
				( !empty( $img_set['medium_post_list_small']['h'] ) ? $img_set['medium_post_list_small']['h'] : 285 )),
			'large_post_list' => array( 
				( !empty( $img_set['large_post_list_small']['w'] ) ? $img_set['large_post_list_small']['w'] : 450 ),
				( !empty( $img_set['large_post_list_small']['h'] ) ? $img_set['large_post_list_small']['h'] : 279 )),

			'portfolio_single_full' => array( 
				( !empty( $img_set['portfolio_single_full_small']['w'] ) ? $img_set['portfolio_single_full_small']['w'] : 690 ),
				( !empty( $img_set['portfolio_single_full_small']['h'] ) ? $img_set['portfolio_single_full_small']['h'] : 475 )),
			'additional_posts_grid' => array( 
				( !empty( $img_set['additional_posts_grid_small']['w'] ) ? $img_set['additional_posts_grid_small']['w'] : 180 ),
				( !empty( $img_set['additional_posts_grid_small']['h'] ) ? $img_set['additional_posts_grid_small']['h'] : 124 )),
				
		);


		$additional_images = array(
		    'one_column_portfolio' => array( 
		        ( !empty( $img_set['one_column_portfolio_fancy']['w'] ) ? $img_set['one_column_portfolio_fancy']['w'] : 980 ),
		        ( !empty( $img_set['one_column_portfolio_fancy']['h'] ) ? $img_set['one_column_portfolio_fancy']['h'] : 675 )),
		    'two_column_portfolio' => array( 
		        ( !empty( $img_set['two_column_portfolio_fancy']['w'] ) ? $img_set['two_column_portfolio_fancy']['w'] : 485 ),
		        ( !empty( $img_set['two_column_portfolio_fancy']['h'] ) ? $img_set['two_column_portfolio_fancy']['h'] : 334 )),
		    'three_column_portfolio' => array( 
		        ( !empty( $img_set['three_column_portfolio_fancy']['w'] ) ? $img_set['three_column_portfolio_fancy']['w'] : 320 ),
		        ( !empty( $img_set['three_column_portfolio_fancy']['h'] ) ? $img_set['three_column_portfolio_fancy']['h'] : 224 )),
		    'four_column_portfolio' => array( 
		        ( !empty( $img_set['four_column_portfolio_fancy']['w'] ) ? $img_set['four_column_portfolio_fancy']['w'] : 237 ),
		        ( !empty( $img_set['four_column_portfolio_fancy']['h'] ) ? $img_set['four_column_portfolio_fancy']['h'] : 164 )),

		    'one_column_blog_full' => array( 
		        ( !empty( $img_set['one_column_blog_fancy_full']['w'] ) ? $img_set['one_column_blog_fancy_full']['w'] : 980 ),
		        ( !empty( $img_set['one_column_blog_fancy_full']['h'] ) ? $img_set['one_column_blog_fancy_full']['h'] : 490 )),
		    'two_column_blog_full' => array( 
		        ( !empty( $img_set['two_column_blog_fancy_full']['w'] ) ? $img_set['two_column_blog_fancy_full']['w'] : 485 ),
		        ( !empty( $img_set['two_column_blog_fancy_full']['h'] ) ? $img_set['two_column_blog_fancy_full']['h'] : 242 )),

		    'one_column_blog_big' => array( 
		        ( !empty( $img_set['one_column_blog_fancy_big']['w'] ) ? $img_set['one_column_blog_fancy_big']['w'] : 720 ),
		        ( !empty( $img_set['one_column_blog_fancy_big']['h'] ) ? $img_set['one_column_blog_fancy_big']['h'] : 360 )),
		    'two_column_blog_big' => array( 
		        ( !empty( $img_set['two_column_blog_fancy_big']['w'] ) ? $img_set['two_column_blog_fancy_big']['w'] : 355 ),
		        ( !empty( $img_set['two_column_blog_fancy_big']['h'] ) ? $img_set['two_column_blog_fancy_big']['h'] : 177 )),

		    'one_column_blog_small' => array( 
		        ( !empty( $img_set['one_column_blog_fancy_small']['w'] ) ? $img_set['one_column_blog_fancy_small']['w'] : 750 ),
		        ( !empty( $img_set['one_column_blog_fancy_small']['h'] ) ? $img_set['one_column_blog_fancy_small']['h'] : 375 )),
		    'two_column_blog_small' => array( 
		        ( !empty( $img_set['two_column_blog_fancy_small']['w'] ) ? $img_set['two_column_blog_fancy_small']['w'] : 370 ),
		        ( !empty( $img_set['two_column_blog_fancy_small']['h'] ) ? $img_set['two_column_blog_fancy_small']['h'] : 185 )),
		
			'image_banner_intro' => array( 
		        ( !empty( $img_set['image_banner_intro_full']['w'] ) ? $img_set['image_banner_intro_full']['w'] : 980 ),
		        ( !empty( $img_set['image_banner_intro_full']['h'] ) ? $img_set['image_banner_intro_full']['h'] : 470 )),

			);

		# Slider
		$images_slider = array(
			'responsive_slide' => array( 980, 470 ),
			'nivo_slide' => array( 980, 470 ),
			'floating_slide' => array( 980, 430 ),
			'staged_slide' => array( 970, 458 ),
			'partial_staged_slide' => array( 550, 380 ),
			'partial_gradient_slide' => array( 980, 470 ),
			'overlay_slide' => array( 980, 470 ),
			'full_slide' => array( 980, 470 ),
			'nav_thumbs' => array( 45, 30 )
		);
		
		foreach( $images as $key => $value ) {
			foreach( $value as $img => $size ) {
				$size = str_replace( ' ', '', $size );
				$new_size[$img] = str_replace( 'px', '', $size );
			}
			$images[$key] = $new_size;
		}

		foreach( $big_sidebar_images as $key => $value ) {
			foreach( $value as $img => $size ) {
				$size = str_replace( ' ', '', $size );
				$new_size[$img] = str_replace( 'px', '', $size );
			}
			$big_sidebar_images[$key] = $new_size;
		}

		foreach( $small_sidebar_images as $key => $value ) {
			foreach( $value as $img => $size ) {
				$size = str_replace( ' ', '', $size );
				$new_size[$img] = str_replace( 'px', '', $size );
			}
			$small_sidebar_images[$key] = $new_size;
		}
		
		foreach( $additional_images as $key => $value ) {
			foreach( $value as $img => $size ) {
				$size = str_replace( ' ', '', $size );
				$new_size[$img] = str_replace( 'px', '', $size );
			}
			$additional_images[$key] = $new_size;
		}
		
		# Blog layouts
		switch( $blog_layout ) {
			case "blog_layout1":
				$layout = array(
					'blog_layout' => $blog_layout,
					'main_class' => 'post_grid blog_layout1',
					'post_class' => 'post_grid_module',
					'content_class' => 'post_grid_content',
					'img_class' => 'post_grid_image'
				);
				break;
			case "blog_layout2":
				$layout = array(
					'blog_layout' => $blog_layout,
					'main_class' => 'post_list blog_layout2',
					'post_class' => 'post_list_module',
					'content_class' => 'post_list_content',
					'img_class' => 'post_list_image'
				);
				break;
			case "blog_layout3":
				$columns_num = 2;
				$featured = 1;
				$columns = ( $columns_num == 2 ? 'one_half'
				: ( $columns_num == 3 ? 'one_third'
				: ( $columns_num == 4 ? 'one_fourth'
				: ( $columns_num == 5 ? 'one_fifth'
				: ( $columns_num == 6 ? 'one_sixth'
				: ''
				)))));

				$layout = array(
					'blog_layout' => $blog_layout,
					'main_class' => 'post_grid blog_layout3',
					'post_class' => 'post_grid_module',
					'content_class' => 'post_grid_content',
					'img_class' => 'post_grid_image',
					'columns_num' => ( !empty( $columns_num ) ? $columns_num : '' ),
					'featured' => ( !empty( $featured ) ? $featured : '' ),
					'columns' => ( !empty( $columns ) ? $columns : '' )
				);
				break;
		}

		$mysite->layout['blog'] = $layout;
		$mysite->layout['images'] = array_merge( $images, array( 'image_padding' => 0 ) );
		$mysite->layout['big_sidebar_images'] = $big_sidebar_images;
		$mysite->layout['small_sidebar_images'] = $small_sidebar_images;
		$mysite->layout['additional_images'] = $additional_images;
		$mysite->layout['images_slider'] = $images_slider;
	}
	
}


/**
 * Functions & Pluggable functions specific to theme.
 *
 * @package BackStopThemes
 * @subpackage Fusion
 */

if ( !function_exists( 'mysite_post_img_overlay' ) ) :
/**
 *
 */
function mysite_post_img_overlay() {
	$out = '<span class="image_overlay"></span>';
	echo $out;
}
endif;

if ( !function_exists( 'mysite_fancy_link' ) ) :
/**
 *
 */
function mysite_fancy_link( $fancy_link, $args ) {
	extract( $args );
	return '<span class="fancy_link"><a href="' . esc_url( $link ) . '" class="fancy_link_a' . $target . $variation . '"' . $color .'>' . mysite_remove_wpautop( $content ) . '</a><span class="fancy_link_arrow"></span></span>';
}
endif;

if ( !function_exists( 'mysite_teaser_single_title' ) ) :
/**
 *
 */
function mysite_teaser_single_title() {
	global $post;
	
	if( !empty( $post->ID ) )
		return get_the_title( $post->ID );
}
endif;

if ( !function_exists( 'mysite_entry_title' ) ) :
/**
 *
 */
function mysite_entry_title( $out ) {
	if( is_singular( 'post' ) && empty( $out ) )
		add_action( 'mysite_singular-post', 'mysite_post_title' );
	
	return str_replace( '<div id="intro">', '<div id="intro" class="noscript">', $out );
	
}
endif;

if ( !function_exists( 'mysite_fancy_read_more' ) ) :
/**
 *
 */
function mysite_fancy_read_more( $args = array() ) {
	global $mysite;
	
	if( is_single() ) return;
	
	extract( $args );
	
	$out = '';
	
	if( $thumb != 'small' )
		$out .= '<div class="fancy_read_more" style="display:none;"><span><a href="' . get_permalink() . '" class="post_more_link">' . __( 'Read More', 'backstop-themes' ) . '</a></span></div>';
	
	echo apply_atomic_shortcode( 'fancy_read_more', $out );
}
endif;

if ( !function_exists( 'mysite_portfolio_meta' ) ) :
/**
 *
 */
function mysite_portfolio_meta( $args ) {
	extract( $args );
	
	$out = '';
	
	if( empty( $more ) || !empty( $_link[$id] ) ) {
		
		$out .= '<div class="fancy_read_more" style="display:none;">';
		$out .= '<span>';
		
		if( ( !empty( $link ) ) && ( strpos( $disable, 'visit' ) === false ) ) {
			$visit_site = '<a href="' . esc_url( $link )  . '" class="post_more_link visit_site_link">' . __( 'Visit Site', 'backstop-themes' ) . '</a>';
			$out .= apply_filters( 'fusion_portfolio_visit_site', $visit_site, esc_url( $link ) );
		}
		
		if( ( empty( $more ) ) && ( strpos( $disable, 'more' ) === false ) ) {
			$read_more = '<a href="' . esc_url( $url )  . '" class="post_more_link">' . __( 'Read More', 'backstop-themes' ) . '</a>';
			$out .= apply_filters( 'fusion_portfolio_read_more', $read_more, esc_url( $url ) );
		}
		
		$out .= '</span>';
		$out .= '</div>';
	}

	echo apply_atomic( 'portfolio_fancy_read_more', $out );
}
endif;

if ( !function_exists( 'mysite_post_meta' ) ) :
/**
 *
 */
function mysite_post_meta( $args = array() ) {
	$defaults = array(
		'shortcode' => false,
		'echo' => true
	);
	
	$args = wp_parse_args( $args, $defaults );
	
	extract( $args );
	
	if( ( is_page() && !$shortcode ) || is_singular( 'post' ) || is_singular( 'portfolio' ) ) return;
	
	$out = '';
	$meta_options = mysite_get_setting( 'disable_meta_options' );
	$_meta = ( is_array( $meta_options ) ) ? $meta_options : array();
	$meta_output = '';
	
	if( !in_array( 'date_meta', $_meta ) )
		$meta_output .= '[post_date text="' . __( '<em>Posted on:</em>', 'backstop-themes' ) . ' "] ';
		
	if( !in_array( 'comments_meta', $_meta ) )
		$meta_output .= '[post_comments text="' . __( '<em>With:</em>', 'backstop-themes' ) . ' "] ';
		
	if( !in_array( 'author_meta', $_meta ) )
		$meta_output .= '[post_author text="' . __( '<em>Posted by:</em>', 'backstop-themes' ) . ' "] ';
	
	if( !empty( $meta_output ) )
		$out .='<p class="post_meta">' . $meta_output . '</p>';
	
	if( $echo )
		echo apply_atomic_shortcode( 'post_meta', $out );
	else
		return apply_atomic_shortcode( 'post_meta', $out );
}
endif;

if ( !function_exists( 'mysite_post_meta_single' ) ) :
/**
 *
 */
function mysite_post_meta_single( $args ) {
	global $author, $post;
	
	extract( $args );
	
	if( !is_singular( 'post' ) ) return;
	
	if( $text || $raw ) return;
	
	$out = '';
	$meta_options = mysite_get_setting( 'disable_meta_options' );
	$_meta = ( is_array( $meta_options ) ) ? $meta_options : array();
	$meta_output = '';
	
	if( !in_array( 'date_meta', $_meta ) )
		$meta_output .= '[post_date text="' . __( '<em>Posted on:</em>', 'backstop-themes' ) . ' "] ';
		
	if( !in_array( 'comments_meta', $_meta ) )
		$meta_output .= '[post_comments text="' . __( '<em>With:</em>', 'backstop-themes' ) . ' "] ';
		
	if( !in_array( 'author_meta', $_meta ) && isset( $post->post_author ) ) {
		$meta_output .= '<span class="meta_author">' . __( '<em>Posted by:</em>', 'backstop-themes' ) . ' <a href="'
		. get_author_posts_url( get_the_author_meta( 'ID', intval($post->post_author) ) ) . '">'
		. get_the_author_meta( 'display_name', intval($post->post_author) ) . '</a></span>';
	}
		
	if( !empty( $meta_output ) )
		$out .='<p class="post_meta">' . $meta_output . '</p>';
	
	echo apply_atomic_shortcode( 'post_meta_single', $out );
}
endif;

if ( !function_exists( 'mysite_portfolio_date_top' ) ) :
/**
 *
 */
function mysite_portfolio_date_top( $content, $args ) {
	extract( $args );
	
	$out = '';
	
	if( ( !empty( $date ) ) && ( strpos( $disable, 'date' ) === false ) )
		$out .= '<p class="date">' . $date . '</p>';
	
	return $out;
}
endif;

if ( !function_exists( 'mysite_before_post_sc' ) ) :
/**
 *
 */
function mysite_before_post_sc( $filter_args ) {
	$out = '';
	
	if( strpos( $filter_args['disable'], 'image' ) === false )
		$out .= mysite_get_post_image( $filter_args );
	
	return $out;
}
endif;

if ( !function_exists( 'mysite_before_entry_sc' ) ) :
/**
 *
 */
function mysite_before_entry_sc( $filter_args ) {
	$out = '';
	
	extract( $filter_args );
	
	if( strpos( $disable, 'meta' ) === false )
		$out .= mysite_post_meta( $filter_args );
	
	if( strpos( $disable, 'title' ) === false )
		$out .= mysite_post_title( $filter_args );
	
	return $out;
}
endif;

if ( !function_exists( 'mysite_excerpt_more' ) ) :
/**
 *
 */
function mysite_excerpt_more( $more ) {
	return ' ...';
}
endif;

if ( !function_exists( 'mysite_comments_callback' ) ) :
/**
 *
 */
function mysite_comments_callback( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;

	$comment_type = get_comment_type( $comment->comment_ID );
	$author = esc_html( get_comment_author( $comment->comment_ID ) );
	$url = esc_url( get_comment_author_url( $comment->comment_ID ) );
	$default_avatar = ( 'pingback' == $comment_type || 'trackback' == $comment_type )
	? THEME_IMAGES_ASSETS . "/gravatar_{$comment_type}.png"
	: THEME_IMAGES_ASSETS . '/gravatar_default.png';

	?><li <?php comment_class() ?> id="comment-<?php comment_ID() ?>">
		<div id="div-comment-<?php comment_ID() ?>"><?php

		/* Display gravatar */
		$avatar = get_avatar( get_comment_author_email( $comment->comment_ID ), apply_filters( "mysite_avatar_size", '80' ), $default_avatar, $author );

		if ( $url )
			$avatar = '<a href="' . esc_url( $url ) . '" rel="external nofollow" title="' . esc_attr( $author ) . '">' . $avatar . '</a>';

		echo $avatar;

		?><div class="comment-text"><?php

		/* Display link and cite if URL is set. */
		if ( $url )
			echo '<cite class="fn" title="' . esc_url( $url ) . '"><a href="' . esc_url( $url ) . '" title="' . esc_attr( $author ) . '" class="url" rel="external nofollow">' . $author . '</a></cite>';
		else
			echo '<cite class="fn">' . $author . '</cite>';
			
		?><div class="comment-meta commentmetadata"><?php
			comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) );
			edit_comment_link( __( 'Edit', 'backstop-themes' ), ' ' );
		?></div><?php

		/* Display comment date */
		?><span class="date"><?php printf( __('%1$s', 'backstop-themes' ), get_comment_date( get_option('date_format') ) ); ?></span>

		<?php if ( $comment->comment_approved == '0' ) : ?>
			<p class="alert moderation"><?php _e( 'Your comment is awaiting moderation.', 'backstop-themes' ); ?></p>
				<?php endif; ?>

				<?php comment_text() ?>

			</div><!-- .comment-text -->

		</div><!-- #div-comment-## -->
<?php
}
endif;

if ( !function_exists( 'mysite_intro_javascript' ) ) :
/**
 *
 */
function mysite_intro_javascript() {
	
	$out = "
	<script type=\"text/javascript\">
	/* <![CDATA[ */
	jQuery(document).ready(function() {
		var introInnerHeight = jQuery('#intro_inner').css('height');

		var introTitleHeight = jQuery('.intro_title').css('height'),
			introTitlePaddingTop = jQuery('.intro_title').css('padding-top'),
			introTitlePaddingBottom = jQuery('.intro_title').css('padding-bottom');

		var teaserHeight = jQuery('#intro_inner .teaser').css('height'),
			teaserPaddingTop = jQuery('#intro_inner .teaser').css('padding-top'),
			teaserPaddingBottom = jQuery('#intro_inner .teaser').css('padding-bottom');

		var introTitleTotalHeight = parseFloat(introTitleHeight) + parseFloat(introTitlePaddingTop) + parseFloat(introTitlePaddingBottom),
			teaserTotalHeight = parseFloat(teaserHeight) + parseFloat(teaserPaddingTop) + parseFloat(teaserPaddingBottom);

		if( introTitleTotalHeight < parseFloat(introInnerHeight) ){
			var introTitleInlinePadding = parseFloat(introInnerHeight) - parseFloat(introTitleTotalHeight) + parseFloat(introTitlePaddingBottom);
			jQuery('.intro_title').css('padding-bottom', introTitleInlinePadding);
		} 

		if( (parseFloat(introInnerHeight) - teaserTotalHeight) > parseFloat(teaserPaddingTop) ){
			var introTeaserInlinePadding = parseFloat(introInnerHeight) - parseFloat(teaserTotalHeight);
			jQuery('.teaser').css('padding-top', introTeaserInlinePadding);
		}

		if(jQuery('#intro_inner .post_meta').length>0){
			var postMetaHeight = jQuery('#intro_inner .post_meta').css('height'),
				postMetaInlinePadding = parseFloat(introInnerHeight) - parseFloat(postMetaHeight);

			jQuery('#intro_inner .post_meta').css('padding-top', postMetaInlinePadding);
		}

		jQuery('#intro').removeClass('noscript');
	});
	/* ]]> */
	</script>";

	echo preg_replace( "/(\r\n|\r|\n)\s*/i", '', $out );
	
}
endif;

if ( !function_exists( 'mysite_image_preloading' ) ) :
/**
 *
 */
function mysite_image_preloading() {
	global $mysite;

	if( isset( $mysite->mobile ) )
		return;
	
	$out = "
	<script type=\"text/javascript\">
	/* <![CDATA[ */
	
	jQuery( '#main_inner' ).preloader({ imgSelector: '.blog_index_image_load span img', imgAppend: '.blog_index_image_load', oneachload: function(image) { jQuery(image).parent().parent().next().fadeIn(200); } });
	
	jQuery( '.one_column_portfolio' ).preloader({ imgSelector: '.portfolio_img_load span img', imgAppend: '.portfolio_img_load', oneachload: function(image) { jQuery(image).parent().parent().next().fadeIn(200); } });
	jQuery( '.two_column_portfolio' ).preloader({ imgSelector: '.portfolio_img_load span img', imgAppend: '.portfolio_img_load', oneachload: function(image) { jQuery(image).parent().parent().next().fadeIn(200); } });
	jQuery( '.three_column_portfolio' ).preloader({ imgSelector: '.portfolio_img_load span img', imgAppend: '.portfolio_img_load', oneachload: function(image) { jQuery(image).parent().parent().next().fadeIn(200); } });
	jQuery( '.four_column_portfolio' ).preloader({ imgSelector: '.portfolio_img_load span img', imgAppend: '.portfolio_img_load', oneachload: function(image) { jQuery(image).parent().parent().next().fadeIn(200); } });
	
	jQuery( '.portfolio_gallery.large_post_list' ).preloader({ imgSelector: '.portfolio_img_load span img', imgAppend: '.portfolio_img_load', oneachload: function(image) { jQuery(image).parent().parent().next().fadeIn(200); } });
	jQuery( '.portfolio_gallery.medium_post_list' ).preloader({ imgSelector: '.portfolio_img_load span img', imgAppend: '.portfolio_img_load', oneachload: function(image) { jQuery(image).parent().parent().next().fadeIn(200); } });
	jQuery( '.portfolio_gallery.small_post_list' ).preloader({ imgSelector: '.portfolio_img_load span img', imgAppend: '.portfolio_img_load' });
	
	jQuery( '#main_inner' ).preloader({ imgSelector: '.portfolio_full_image span img', imgAppend: '.portfolio_full_image' });
	jQuery( '#main_inner' ).preloader({ imgSelector: '.blog_sc_image_load span img', imgAppend: '.blog_sc_image_load' , oneachload: function(image) { jQuery(image).parent().parent().next().fadeIn(200); } });
	jQuery( '#main_inner, #sidebar_inner' ).preloader({ imgSelector: '.fancy_image_load span img', imgAppend: '.fancy_image_load', oneachload: function(image){
			var imageCaption = jQuery(image).parent().parent().next();
			if(imageCaption.length>0){
				imageCaption.remove();
				jQuery(image).parent().addClass('has_caption_frame');
				jQuery(image).parent().append(imageCaption);
				jQuery(image).next().css('display','block');
			}
		}
	});
	jQuery( '#intro_inner' ).preloader({ imgSelector: '.fancy_image_load span img', imgAppend: '.fancy_image_load', oneachload: function(image){
			var imageCaption = jQuery(image).parent().parent().next();
			if(imageCaption.length>0){
				imageCaption.remove();
				jQuery(image).parent().addClass('has_caption_frame');
				jQuery(image).parent().append(imageCaption);
				jQuery(image).next().css('display','block');
			}
		}
	});
	
	function mysite_jcarousel_setup(c) {
		c.clip.parent().parent().parent().parent().parent().removeClass('noscript');
		var jcarousel_img_load = c.clip.children().children().find('.post_grid_image .portfolio_img_load');
		if( jcarousel_img_load.length>1 ) {
			jcarousel_img_load.each(function(i) {
				var filename = jQuery(this).attr('href'),
					videos=['swf','youtube','vimeo','mov'];
				for(var v in videos){
				    if(filename.match(videos[v])){
						jQuery(this).css('backgroundImage','url(' +assetsUri+ '/play.png)');
					}else{
						jQuery(this).css('backgroundImage','url(' +assetsUri+ '/zoom.png)');
					}
				}
				jQuery(this).next().fadeIn(200);
			});
		}
	}
	
	/* ]]> */
	</script>";

	echo preg_replace( "/(\r\n|\r|\n)\s*/i", '', $out );
}
endif;

?>