<?php
/**
 * Archive Template
 *
 * @package BackStopThemes
 * @subpackage Template
 */

get_header(); ?>

	<?php mysite_archive(); ?>
	
	<?php mysite_after_page_content(); ?>

			<div class="clearboth"></div>
		</div><!-- #main_inner -->
	</div><!-- #main -->

<?php get_footer(); ?>