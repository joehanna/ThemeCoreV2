| Documentation Index             | Details                                                                                                     |
| --------------------------------|-------------------------------------------------------------------------------------------------------------|
| **[General Issue Forum][1]**    | A community Question & Answer Forum for general issues related to the BackStopThemes initiative             |
| **[Overview][2]**               | One page overview of the vision, content and structure of the BackStop Themes Suite (this page)             |
| **[Development History][3]**    | A brief history of the evolution of BackStop Themes                                                         |

[1]: https://gitlab.com/BackStopThemes/GeneralIssuesForum/
[2]: https://gitlab.com/BackStopThemes/ThemeCoreV2/
[3]: https://gitlab.com/BackStopThemes/ThemeCoreV2/wikis/development-history/

------------------------------------------------------------------------------------------------------------------------------------------

## Theme Design Overview

The BackStop Themes are a combination of 3 elements: ThemeCore + SkinFrame + Skin. The first component includes the common components for a WordPress theme, the second component includes the structure for a specific theme look & feel and the third component includes the specific theme look & feel.

------------------------------------------------------------------------------------------------------------------------------------------

### Theme Build & Installation Instructions

To build this theme simply download the files in this project and the files in the ThemeCoreV2 project, remove the git directories and combine them to form a single folder on your computer. Zip the folder and follow [the standard WordPress theme installation instructions](https://codex.wordpress.org/Using_Themes).

------------------------------------------------------------------------------------------------------------------------------------------

### License

GPLv2 (or later)

------------------------------------------------------------------------------------------------------------------------------------------

### Terms & Conditions

THE SERVICES ARE PROVIDED AS IS WITHOUT ANY REPRESENTATION OR WARRANTY OF ANY KIND, WHETHER EXPRESS, IMPLIED, STATUTORY OR OTHERWISE WITH RESPECT TO THE SERVICE. EXCEPT TO THE EXTENT PROHIBITED BY LAW, WE DISCLAIM ALL WARRANTIES INCLUDING IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR PURPOSE AND NON-INFRINGEMENT.
NEITHER WE NOR OUR LICENSORS SHALL BE LIABLE TO YOU FOR ANY DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR EXEMPLARY DAMAGES INCLUDING BUT NOT LIMITED TO DAMAGES FOR LOSS OF PROFITS OR GOODWILL EVEN IF SUCH LOSS OR DAMAGE WAS REASONABLY FORSEEABLE.

By forking this software you agree to these terms & conditions. These terms may be adjusted from time to time so please check here periodically for any updates. If you do not agree to the modified terms, you should discontinue your use of this software.

------------------------------------------------------------------------------------------------------------------------------------------

### Credits

Webtreats / MySiteMyWay is the founding creator of this open source theme software forked to create BackStop Themes.

The SkyBind division of OnePressTech Pty Ltd. is the Founder of BackStop Themes and the manager of the BackStop Themes software evolution along with the BackStop Themes community. OnePressTech is a Managed Office Cloud provider to small businesses, NGOs, community groups, and small departments in mid-sized and large organsisations.

For a brief history of the evolution of this WordPress theme suite [click here][2].