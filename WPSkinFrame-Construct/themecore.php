<?php
/**
 * The BackStopThemes class. Defines the necessary constants 
 * and includes the necessary files for theme's operation.
 *
 * @package BackStopThemes
 * @subpackage Construct
 */

class BackStopThemes extends BackStopThemesCommon {
	
	public function load_theme_specific_constants( $options ) {
	}

	/**
	 * Loads theme actions.
	 *
	 * @since 1.0
	 */
	public function load_theme_actions() {
		
		# WordPress actions
		add_action( 'init', 'mysite_is_mobile_device' );
		add_action( 'init', 'mysite_is_responsive' );
		add_action( 'init', 'mysite_shortcodes_init' );
		add_action( 'init', 'mysite_menus' );
		add_action( 'init', 'mysite_post_types'  );
		add_action( 'init', 'mysite_register_script' );
		add_action( 'init', 'mysite_wp_image_resize', 11 );
		add_action( 'init', array( 'mysiteForm', 'init'), 11 );
		add_action( 'widgets_init', 'mysite_sidebars' );
		add_action( 'widgets_init', 'mysite_widgets' );
		add_action( 'wp_head', 'mysite_seo_meta' );
		add_action( 'wp_head', 'mysite_mobile_meta' );
		add_action( 'wp_head', 'mysite_custom_js_hover' );
		add_action( 'wp_head', 'mysite_analytics' );
		add_action( 'wp_head', 'mysite_custom_bg' );
		add_action( 'wp_head', 'mysite_additional_headers', 99 );
		add_action( 'wp_head', 'mysite_fitvids' );
		add_action( 'template_redirect', 'mysite_enqueue_script' );
		add_action( 'template_redirect', 'mysite_squeeze_page' );
		add_action( 'comment_form_defaults', 'mysite_comment_form_args' );
		remove_action( 'wp_head', 'rel_canonical' );
		
		# BackStopThemes actions
		add_action( 'mysite_head', 'mysite_header_scripts' );
		add_action( 'mysite_before_header', 'mysite_fullscreen_bg' );
		add_action( 'mysite_before_header', 'mysite_header_extras' );
		add_action( 'mysite_before_header', create_function('','echo "<div id=\"content_wrap\">";') );
		
		add_action( 'mysite_header', 'mysite_logo' );
		add_action( 'mysite_header', 'mysite_primary_menu' );
		add_action( 'mysite_header', 'mysite_responsive_menu' );
		
		add_action( 'mysite_after_header', 'mysite_slider_module' );
		add_action( 'mysite_after_header', 'mysite_teaser' );
		add_action( 'mysite_after_header', 'mysite_breadcrumbs' );
		
		add_action( 'mysite_before_page_content', 'mysite_home_content' );
		add_action( 'mysite_before_page_content', 'mysite_page_content' );
		add_action( 'mysite_before_page_content', 'mysite_page_title' );
		add_action( 'mysite_before_page_content', 'mysite_query_posts' );
		
		add_action( 'mysite_before_post', 'mysite_post_image' );
		add_action( 'mysite_before_entry', 'mysite_post_title' );
		add_action( 'mysite_before_entry', 'mysite_post_meta' );
		
		add_action( 'mysite_singular-page_before_entry', 'mysite_post_image' );
		add_action( 'mysite_singular-post_after_entry', 'mysite_post_meta_bottom' );

		add_action( 'mysite_singular-post_after_entry', 'mysite_post_sociables' );
		add_action( 'mysite_singular-post_after_post', 'mysite_post_nav' );		
		add_action( 'mysite_singular-post_after_post', 'mysite_about_author' );
		add_action( 'mysite_singular-post_after_post', 'mysite_like_module' );
		add_action( 'mysite_singular-portfolio_after_post', 'mysite_post_sociables' );

		add_action( 'mysite_after_post', 'mysite_page_navi' );
		add_action( 'mysite_after_main', 'mysite_get_sidebar' );
		add_action( 'mysite_before_footer', 'mysite_footer_teaser' );
		add_action( 'mysite_footer', 'mysite_main_footer' );

		add_action( 'mysite_after_footer', create_function('','echo "</div>";') );
		add_action( 'mysite_after_footer', 'mysite_sub_footer' );
		add_action( 'mysite_body_end', 'mysite_print_cufon' );
		add_action( 'mysite_body_end', 'mysite_image_preloading' );
		add_action( 'mysite_body_end', 'mysite_ios_rotate' );
		add_action( 'mysite_body_end', 'mysite_custom_javascript' );
	}
	
	/**
	 * Loads theme filters.
	 *
	 * @since 1.0
	 */
	public function load_theme_filters() {
		
		# BackStopThemes filters
		add_filter( 'mysite_avatar_size', create_function('','return "60";') );
		add_filter( 'mysite_author_avatar_size', create_function('','return "60";') );
		add_filter( 'mysite_comment_date_format', create_function('','return __( "F j,y" );') );
		add_filter( 'mysite_read_more', 'mysite_read_more' );
		add_filter( 'mysite_fancy_link', 'mysite_fancy_link', 1, 2 );
		add_filter( 'the_content_more_link', 'mysite_full_read_more', 10, 2 );
		add_filter( 'excerpt_length', 'mysite_excerpt_length_long', 999 );
		add_filter( 'excerpt_more', 'mysite_excerpt_more' );
		add_filter( 'posts_where', 'mysite_multi_tax_terms' );
		add_filter( 'pre_get_posts', 'mysite_exclude_category_feed' );
		add_filter( 'pre_get_posts', 'mysite_custom_search' );
		add_filter( 'widget_categories_args', 'mysite_exclude_category_widget' );
		add_filter( 'query_vars', 'mysite_queryvars' );
		add_filter( 'rewrite_rules_array', 'mysite_rewrite_rules',10,2 );
		add_filter( 'widget_text', 'do_shortcode' );
		add_filter( 'wp_page_menu_args', 'mysite_page_menu_args' );
		add_filter( 'the_password_form', 'mysite_password_form' );
	}
	
	/**
	 * Define theme variables.
	 *
	 * @since 1.0
	 */
	public function load_theme_variables() {
		global $mysite;
		
		$layout = '';
		$img_set = get_option( MYSITE_SETTINGS );
		$img_set = ( !empty( $img_set ) && !isset( $_POST[MYSITE_SETTINGS]['reset'] ) ) ? $img_set : array();
		$blog_layout = apply_filters( 'mysite_blog_layout', mysite_get_setting( 'blog_layout' ) );
		
		# Images
		$images = array(
			'one_column_portfolio' => array( 
				( !empty( $img_set['one_column_portfolio_full']['w'] ) ? $img_set['one_column_portfolio_full']['w'] : 900 ),
				( !empty( $img_set['one_column_portfolio_full']['h'] ) ? $img_set['one_column_portfolio_full']['h'] : 559 )),
			'two_column_portfolio' => array( 
				( !empty( $img_set['two_column_portfolio_full']['w'] ) ? $img_set['two_column_portfolio_full']['w'] : 432 ),
				( !empty( $img_set['two_column_portfolio_full']['h'] ) ? $img_set['two_column_portfolio_full']['h'] : 268 )),
			'three_column_portfolio' => array( 
				( !empty( $img_set['three_column_portfolio_full']['w'] ) ? $img_set['three_column_portfolio_full']['w'] : 276 ),
				( !empty( $img_set['three_column_portfolio_full']['h'] ) ? $img_set['three_column_portfolio_full']['h'] : 171 )),
			'four_column_portfolio' => array( 
				( !empty( $img_set['four_column_portfolio_full']['w'] ) ? $img_set['four_column_portfolio_full']['w'] : 198 ),
				( !empty( $img_set['four_column_portfolio_full']['h'] ) ? $img_set['four_column_portfolio_full']['h'] : 122 )),

			'one_column_blog' => array( 
				( !empty( $img_set['one_column_blog_full']['w'] ) ? $img_set['one_column_blog_full']['w'] : 900 ),
				( !empty( $img_set['one_column_blog_full']['h'] ) ? $img_set['one_column_blog_full']['h'] : 391 )),
			'two_column_blog' => array( 
				( !empty( $img_set['two_column_blog_full']['w'] ) ? $img_set['two_column_blog_full']['w'] : 432 ),
				( !empty( $img_set['two_column_blog_full']['h'] ) ? $img_set['two_column_blog_full']['h'] : 187 )),
			'three_column_blog' => array( 
				( !empty( $img_set['three_column_blog_full']['w'] ) ? $img_set['three_column_blog_full']['w'] : 276 ),
				( !empty( $img_set['three_column_blog_full']['h'] ) ? $img_set['three_column_blog_full']['h'] : 120 )),
			'four_column_blog' => array( 
				( !empty( $img_set['four_column_blog_full']['w'] ) ? $img_set['four_column_blog_full']['w'] : 198 ),
				( !empty( $img_set['four_column_blog_full']['h'] ) ? $img_set['four_column_blog_full']['h'] : 86 )),

			'small_post_list' => array( 
				( !empty( $img_set['small_post_list_full']['w'] ) ? $img_set['small_post_list_full']['w'] : 50 ),
				( !empty( $img_set['small_post_list_full']['h'] ) ? $img_set['small_post_list_full']['h'] : 50 )),
			'medium_post_list' => array( 
				( !empty( $img_set['medium_post_list_full']['w'] ) ? $img_set['medium_post_list_full']['w'] : 200 ),
				( !empty( $img_set['medium_post_list_full']['h'] ) ? $img_set['medium_post_list_full']['h'] : 200 )),
			'large_post_list' => array( 
				( !empty( $img_set['large_post_list_full']['w'] ) ? $img_set['large_post_list_full']['w'] : 588 ),
				( !empty( $img_set['large_post_list_full']['h'] ) ? $img_set['large_post_list_full']['h'] : 365 )),

			'portfolio_single_full' => array( 
				( !empty( $img_set['portfolio_single_full_full']['w'] ) ? $img_set['portfolio_single_full_full']['w'] : 900 ),
				( !empty( $img_set['portfolio_single_full_full']['h'] ) ? $img_set['portfolio_single_full_full']['h'] : 559 )),
			'additional_posts_grid' => array( 
				( !empty( $img_set['additional_posts_grid_full']['w'] ) ? $img_set['additional_posts_grid_full']['w'] : 198 ),
				( !empty( $img_set['additional_posts_grid_full']['h'] ) ? $img_set['additional_posts_grid_full']['h'] : 122 )),

		);

		$big_sidebar_images = array(
			'one_column_portfolio' => array( 
				( !empty( $img_set['one_column_portfolio_big']['w'] ) ? $img_set['one_column_portfolio_big']['w'] : 580 ),
				( !empty( $img_set['one_column_portfolio_big']['h'] ) ? $img_set['one_column_portfolio_big']['h'] : 360 )),
			'two_column_portfolio' => array( 
				( !empty( $img_set['two_column_portfolio_big']['w'] ) ? $img_set['two_column_portfolio_big']['w'] : 278 ),
				( !empty( $img_set['two_column_portfolio_big']['h'] ) ? $img_set['two_column_portfolio_big']['h'] : 172 )),
			'three_column_portfolio' => array( 
				( !empty( $img_set['three_column_portfolio_big']['w'] ) ? $img_set['three_column_portfolio_big']['w'] : 177 ),
				( !empty( $img_set['three_column_portfolio_big']['h'] ) ? $img_set['three_column_portfolio_big']['h'] : 109 )),
			'four_column_portfolio' => array( 
				( !empty( $img_set['four_column_portfolio_big']['w'] ) ? $img_set['four_column_portfolio_big']['w'] : 127 ),
				( !empty( $img_set['four_column_portfolio_big']['h'] ) ? $img_set['four_column_portfolio_big']['h'] : 78 )),

			'one_column_blog' => array( 
				( !empty( $img_set['one_column_blog_big']['w'] ) ? $img_set['one_column_blog_big']['w'] : 580 ),
				( !empty( $img_set['one_column_blog_big']['h'] ) ? $img_set['one_column_blog_big']['h'] : 252 )),
			'two_column_blog' => array( 
				( !empty( $img_set['two_column_blog_big']['w'] ) ? $img_set['two_column_blog_big']['w'] : 278 ),
				( !empty( $img_set['two_column_blog_big']['h'] ) ? $img_set['two_column_blog_big']['h'] : 120 )),
			'three_column_blog' => array( 
				( !empty( $img_set['three_column_blog_big']['w'] ) ? $img_set['three_column_blog_big']['w'] : 177 ),
				( !empty( $img_set['three_column_blog_big']['h'] ) ? $img_set['three_column_blog_big']['h'] : 76 )),
			'four_column_blog' => array( 
				( !empty( $img_set['four_column_blog_big']['w'] ) ? $img_set['four_column_blog_big']['w'] : 127 ),
				( !empty( $img_set['four_column_blog_big']['h'] ) ? $img_set['four_column_blog_big']['h'] : 55 )),

			'small_post_list' => array( 
				( !empty( $img_set['small_post_list_big']['w'] ) ? $img_set['small_post_list_big']['w'] : 50 ),
				( !empty( $img_set['small_post_list_big']['h'] ) ? $img_set['small_post_list_big']['h'] : 50 )),
			'medium_post_list' => array( 
				( !empty( $img_set['medium_post_list_big']['w'] ) ? $img_set['medium_post_list_big']['w'] : 200 ),
				( !empty( $img_set['medium_post_list_big']['h'] ) ? $img_set['medium_post_list_big']['h'] : 200 )),
			'large_post_list' => array( 
				( !empty( $img_set['large_post_list_big']['w'] ) ? $img_set['large_post_list_big']['w'] : 378 ),
				( !empty( $img_set['large_post_list_big']['h'] ) ? $img_set['large_post_list_big']['h'] : 234 )),

			'portfolio_single_full' => array( 
				( !empty( $img_set['portfolio_single_full_big']['w'] ) ? $img_set['portfolio_single_full_big']['w'] : 580 ),
				( !empty( $img_set['portfolio_single_full_big']['h'] ) ? $img_set['portfolio_single_full_big']['h'] : 360 )),
			'additional_posts_grid' => array( 
				( !empty( $img_set['additional_posts_grid_big']['w'] ) ? $img_set['additional_posts_grid_big']['w'] : 127 ),
				( !empty( $img_set['additional_posts_grid_big']['h'] ) ? $img_set['additional_posts_grid_big']['h'] : 78 )),

		);

		$small_sidebar_images = array(
			'one_column_portfolio' => array( 
				( !empty( $img_set['one_column_portfolio_small']['w'] ) ? $img_set['one_column_portfolio_small']['w'] : 680 ),
				( !empty( $img_set['one_column_portfolio_small']['h'] ) ? $img_set['one_column_portfolio_small']['h'] : 422 )),
			'two_column_portfolio' => array( 
				( !empty( $img_set['two_column_portfolio_small']['w'] ) ? $img_set['two_column_portfolio_small']['w'] : 326 ),
				( !empty( $img_set['two_column_portfolio_small']['h'] ) ? $img_set['two_column_portfolio_small']['h'] : 202 )),
			'three_column_portfolio' => array( 
				( !empty( $img_set['three_column_portfolio_small']['w'] ) ? $img_set['three_column_portfolio_small']['w'] : 208 ),
				( !empty( $img_set['three_column_portfolio_small']['h'] ) ? $img_set['three_column_portfolio_small']['h'] : 129 )),
			'four_column_portfolio' => array( 
				( !empty( $img_set['four_column_portfolio_small']['w'] ) ? $img_set['four_column_portfolio_small']['w'] : 149 ),
				( !empty( $img_set['four_column_portfolio_small']['h'] ) ? $img_set['four_column_portfolio_small']['h'] : 92 )),

			'one_column_blog' => array( 
				( !empty( $img_set['one_column_blog_small']['w'] ) ? $img_set['one_column_blog_small']['w'] : 680 ),
				( !empty( $img_set['one_column_blog_small']['h'] ) ? $img_set['one_column_blog_small']['h'] : 295 )),
			'two_column_blog' => array( 
				( !empty( $img_set['two_column_blog_small']['w'] ) ? $img_set['two_column_blog_small']['w'] : 326 ),
				( !empty( $img_set['two_column_blog_small']['h'] ) ? $img_set['two_column_blog_small']['h'] : 141 )),
			'three_column_blog' => array( 
				( !empty( $img_set['three_column_blog_small']['w'] ) ? $img_set['three_column_blog_small']['w'] : 208 ),
				( !empty( $img_set['three_column_blog_small']['h'] ) ? $img_set['three_column_blog_small']['h'] : 90 )),
			'four_column_blog' => array( 
				( !empty( $img_set['four_column_blog_small']['w'] ) ? $img_set['four_column_blog_small']['w'] : 149 ),
				( !empty( $img_set['four_column_blog_small']['h'] ) ? $img_set['four_column_blog_small']['h'] : 64 )),

			'small_post_list' => array( 
				( !empty( $img_set['small_post_list_small']['w'] ) ? $img_set['small_post_list_small']['w'] : 50 ),
				( !empty( $img_set['small_post_list_small']['h'] ) ? $img_set['small_post_list_small']['h'] : 50 )),
			'medium_post_list' => array( 
				( !empty( $img_set['medium_post_list_small']['w'] ) ? $img_set['medium_post_list_small']['w'] : 200 ),
				( !empty( $img_set['medium_post_list_small']['h'] ) ? $img_set['medium_post_list_small']['h'] : 200 )),
			'large_post_list' => array( 
				( !empty( $img_set['large_post_list_small']['w'] ) ? $img_set['large_post_list_small']['w'] : 444 ),
				( !empty( $img_set['large_post_list_small']['h'] ) ? $img_set['large_post_list_small']['h'] : 275 )),

			'portfolio_single_full' => array( 
				( !empty( $img_set['portfolio_single_full_small']['w'] ) ? $img_set['portfolio_single_full_small']['w'] : 680 ),
				( !empty( $img_set['portfolio_single_full_small']['h'] ) ? $img_set['portfolio_single_full_small']['h'] : 422 )),
			'additional_posts_grid' => array( 
				( !empty( $img_set['additional_posts_grid_small']['w'] ) ? $img_set['additional_posts_grid_small']['w'] : 149 ),
				( !empty( $img_set['additional_posts_grid_small']['h'] ) ? $img_set['additional_posts_grid_small']['h'] : 92 )),

		);
		
		$additional_images = array(
		    'image_banner_intro' => array( 
		        ( !empty( $img_set['image_banner_intro_full']['w'] ) ? $img_set['image_banner_intro_full']['w'] : 900 ),
		        ( !empty( $img_set['image_banner_intro_full']['h'] ) ? $img_set['image_banner_intro_full']['h'] : 300 )),
		);



		# Slider
		$images_slider = array(
			'responsive_slide' => array( 900, 400 ),
			'nivo_slide' => array( 900, 400 ),
			'floating_slide' => array( 900, 370 ),
			'staged_slide' => array( 900, 370 ),
			'partial_staged_slide' => array( 570, 370 ),
			'partial_gradient_slide' => array( 540, 400 ),
			'overlay_slide' => array( 900, 400 ),
			'full_slide' => array( 900, 400 ),
			'nav_thumbs' => array( 60, 40 )
		);
		
		foreach( $images as $key => $value ) {
			foreach( $value as $img => $size ) {
				$size = str_replace( ' ', '', $size );
				$new_size[$img] = str_replace( 'px', '', $size );
			}
			$images[$key] = $new_size;
		}

		foreach( $big_sidebar_images as $key => $value ) {
			foreach( $value as $img => $size ) {
				$size = str_replace( ' ', '', $size );
				$new_size[$img] = str_replace( 'px', '', $size );
			}
			$big_sidebar_images[$key] = $new_size;
		}

		foreach( $small_sidebar_images as $key => $value ) {
			foreach( $value as $img => $size ) {
				$size = str_replace( ' ', '', $size );
				$new_size[$img] = str_replace( 'px', '', $size );
			}
			$small_sidebar_images[$key] = $new_size;
		}
		
		foreach( $additional_images as $key => $value ) {
			foreach( $value as $img => $size ) {
				$size = str_replace( ' ', '', $size );
				$new_size[$img] = str_replace( 'px', '', $size );
			}
			$additional_images[$key] = $new_size;
		}
		
		
		# Blog layouts
		switch( $blog_layout ) {
			case "blog_layout1":
				$layout = array(
					'blog_layout' => $blog_layout,
					'main_class' => 'post_grid blog_layout1',
					'post_class' => 'post_grid_module',
					'content_class' => 'post_grid_content',
					'img_class' => 'post_grid_image'
				);
				break;
			case "blog_layout2":
				$layout = array(
					'blog_layout' => $blog_layout,
					'main_class' => 'post_list blog_layout2',
					'post_class' => 'post_list_module',
					'content_class' => 'post_list_content',
					'img_class' => 'post_list_image'
				);
				break;
			case "blog_layout3":
				$columns_num = 2;
				$featured = 1;
				$columns = ( $columns_num == 2 ? 'one_half'
				: ( $columns_num == 3 ? 'one_third'
				: ( $columns_num == 4 ? 'one_fourth'
				: ( $columns_num == 5 ? 'one_fifth'
				: ( $columns_num == 6 ? 'one_sixth'
				: ''
				)))));

				$layout = array(
					'blog_layout' => $blog_layout,
					'main_class' => 'post_grid blog_layout3',
					'post_class' => 'post_grid_module',
					'content_class' => 'post_grid_content',
					'img_class' => 'post_grid_image',
					'columns_num' => ( !empty( $columns_num ) ? $columns_num : '' ),
					'featured' => ( !empty( $featured ) ? $featured : '' ),
					'columns' => ( !empty( $columns ) ? $columns : '' )
				);
				break;
		}

		$mysite->layout['blog'] = $layout;
		$mysite->layout['images'] = array_merge( $images, array( 'image_padding' => 0 ) );
		$mysite->layout['big_sidebar_images'] = $big_sidebar_images;
		$mysite->layout['small_sidebar_images'] = $small_sidebar_images;
		$mysite->layout['additional_images'] = $additional_images;
		$mysite->layout['images_slider'] = $images_slider;
	}
		
}

/**
 * Functions & Pluggable functions specific to theme.
 *
 * @package BackStopThemes
 * @subpackage Construct
 */


if ( !function_exists( 'mysite_fancy_link' ) ) :
/**
 *
 */
function mysite_fancy_link( $fancy_link, $args ) {
	extract( $args );
	$out = '<a href="' . esc_url( $link ) . '" class="fancy_link' . $target . $variation . '"' . $color .'>' . mysite_remove_wpautop( $content ) . '</a>';
	return $out;
}
endif;


if ( !function_exists( 'mysite_read_more' ) ) :
/**
 *
 */
function mysite_read_more( $args = array() ) {
	global $post;
	$out = '<a class="post_more_link" href="' . get_permalink( $post->ID ) . '">' . __( 'Read More', 'backstop-themes' ) . '</a>';
	return $out;
}
endif;


if ( !function_exists( 'mysite_about_author' ) ) :
/**
 *
 */
function mysite_about_author() {
	$disable_post_author = apply_atomic( 'disable_post_author', mysite_get_setting( 'disable_post_author' ) );
	if( !is_singular( 'post' ) || !empty( $disable_post_author ) )
		return;
		
	$out = '';
	
	if( get_the_author_meta( 'description' ) ) {
		$out .= '<div class="about_author_module">';
		$out .= '<div class="about_author_content">';
		
		$out .= get_avatar( get_the_author_meta('user_email'), apply_filters( 'mysite_author_avatar_size', '80' ), THEME_IMAGES_ASSETS . '/author_gravatar_default.png' );
		$out .= '<p class="author_bio"><span class="about_author_title">' . __( 'About the Author', 'backstop-themes' ) . ' : <span class="author_name">' . esc_attr(get_the_author()) . '</span></span>'
		. get_the_author_meta( 'description' );
		
		$out .= '[fancy_link link="' . get_author_posts_url( get_the_author_meta( 'ID' ) ) . '"]' . sprintf( __( 'View all posts by %s', 'backstop-themes' ), get_the_author() ) . '[/fancy_link]';
		$out .= '</p><!-- .author_bio -->';
		
		$out .= '<div class="clearboth"></div>';
		$out .= '</div><!-- .about_author_content -->';
		$out .= '</div><!-- .about_author_module -->';
	}
	
	echo apply_atomic_shortcode( 'about_author', $out );
}
endif;


if ( !function_exists( 'mysite_post_meta' ) ) :
/**
 *
 */
function mysite_post_meta( $args = array() ) {
	$defaults = array(
		'shortcode' => false,
		'echo' => true
	);
	
	$args = wp_parse_args( $args, $defaults );
	
	extract( $args );
	
	if( is_page() && !$shortcode ) return;
	
	$out = '';
	$meta_options = mysite_get_setting( 'disable_meta_options' );
	$_meta = ( is_array( $meta_options ) ) ? $meta_options : array();
	$meta_output = '';
	

	if( !in_array( 'author_meta', $_meta ) )
		$meta_output .= '[post_author text="' . __( '<em>posted by</em>', 'backstop-themes' ) . ' "] ';

	if( !in_array( 'date_meta', $_meta ) )
		$meta_output .= '[post_date format="F j,Y" text="' . __( '<em>on</em>', 'backstop-themes' ) . ' "] ';
		
	if( !in_array( 'date_meta', $_meta ) )
		$meta_output .= '[post_terms taxonomy="category" text="' . __( '<em>in</em>', 'backstop-themes' ) . ' "] ';
	
	if( !empty( $meta_output ) )
		$out .='<p class="post_meta">' . $meta_output . '</p>';
	
	if( $echo )
		echo apply_atomic_shortcode( 'post_meta', $out );
	else
		return apply_atomic_shortcode( 'post_meta', $out );
}
endif;

if ( !function_exists( 'mysite_custom_js_hover' ) ) :
/**
 *
 */
function mysite_custom_js_hover() {
	
	$out = "<script type=\"text/javascript\">
	/* <![CDATA[ */
	jQuery(document).ready(function() {
		jQuery(document).delegate('.construct_hover_fade_js', 'mouseenter mouseleave', function(e) {
			_this = jQuery(this);
			if( e.type == 'mouseenter'){
				_this.find('.hover_overlay').fadeIn(300);
			}
			if( e.type == 'mouseleave'){
				_this.find('.hover_overlay').fadeOut('fast');
			}
		});
		
	});
	/* ]]> */
	</script>";
	
	echo preg_replace( "/(\r\n|\r|\n)\s*/i", '', $out ) . "\r";
}
endif;

if ( !function_exists( 'mysite_image_preloading' ) ) :
/**
 *
 */
function mysite_image_preloading() {
	global $mysite;
	
	if( isset( $mysite->mobile ) )
		return;
	
	$out = "
	<script type=\"text/javascript\">
	/* <![CDATA[ */
	
	jQuery( '#main_inner' ).preloader({ imgSelector: '.blog_index_image_load span img', imgAppend: '.blog_index_image_load',
		oneachload: function(image){
			var img = jQuery(image),
				link = img.parent().parent();
				
			img.removeClass('hover_fade_js');
			link.addClass('construct_hover_fade_js');
			link.prepend('<span class=\"hover_overlay\"><span class=\"hover_icon\"></span></span>');
			if(link.css('backgroundImage').search('play.png')>0){
				link.find('.hover_icon').css('backgroundImage','url(' +assetsUri+ '/play.png)');
			}
			link.css('backgroundImage','none');
		}
	});
	
	jQuery( '.one_column_portfolio' ).preloader({ imgSelector: '.portfolio_img_load span img', imgAppend: '.portfolio_img_load',
		oneachload: function(image){
			var img = jQuery(image),
				post = img.parent().parent().parent().parent(),
				link = img.parent().parent();
			
			img.removeClass('hover_fade_js');
			link.addClass('construct_hover_fade_js');
			post.find('.date').css('display','block');
			link.prepend('<span class=\"hover_overlay\"><span class=\"hover_icon\"></span></span>');
			if(link.css('backgroundImage').search('play.png')>0){
				link.find('.hover_icon').css('backgroundImage','url(' +assetsUri+ '/play.png)');
			}
			if(link.css('backgroundImage')=='none'){
				link.find('.hover_icon').css('backgroundImage','url(' +assetsUri+ '/link.png)');
			}
			link.css('backgroundImage','none');
		}
	});
	
	jQuery( '.two_column_portfolio' ).preloader({ imgSelector: '.portfolio_img_load span img', imgAppend: '.portfolio_img_load',
		oneachload: function(image){
			var img = jQuery(image),
				post = img.parent().parent().parent().parent(),
				link = img.parent().parent();
			
			img.removeClass('hover_fade_js');
			link.addClass('construct_hover_fade_js');
			post.find('.date').css('display','block');
			link.prepend('<span class=\"hover_overlay\"><span class=\"hover_icon\"></span></span>');
			if(link.css('backgroundImage').search('play.png')>0){
				link.find('.hover_icon').css('backgroundImage','url(' +assetsUri+ '/play.png)');
			}
			if(link.css('backgroundImage')=='none'){
				link.find('.hover_icon').css('backgroundImage','url(' +assetsUri+ '/link.png)');
			}
			link.css('backgroundImage','none');
		}
	});
	
	jQuery( '.three_column_portfolio' ).preloader({ imgSelector: '.portfolio_img_load span img', imgAppend: '.portfolio_img_load',
		oneachload: function(image){
			var img = jQuery(image),
				post = img.parent().parent().parent().parent(),
				link = img.parent().parent();
			
			img.removeClass('hover_fade_js');
			link.addClass('construct_hover_fade_js');
			post.find('.date').css('display','block');
			link.prepend('<span class=\"hover_overlay\"><span class=\"hover_icon\"></span></span>');
			if(link.css('backgroundImage').search('play.png')>0){
				link.find('.hover_icon').css('backgroundImage','url(' +assetsUri+ '/play.png)');
			}
			if(link.css('backgroundImage')=='none'){
				link.find('.hover_icon').css('backgroundImage','url(' +assetsUri+ '/link.png)');
			}
			link.css('backgroundImage','none');
		}
	});
	
	jQuery( '.four_column_portfolio' ).preloader({ imgSelector: '.portfolio_img_load span img', imgAppend: '.portfolio_img_load',
		oneachload: function(image){
			var img = jQuery(image),
				post = img.parent().parent().parent().parent(),
				link = img.parent().parent();
			
			img.removeClass('hover_fade_js');
			link.addClass('construct_hover_fade_js');
			post.find('.date').css('display','block');
			link.prepend('<span class=\"hover_overlay\"><span class=\"hover_icon\"></span></span>');
			if(link.css('backgroundImage').search('play.png')>0){
				link.find('.hover_icon').css('backgroundImage','url(' +assetsUri+ '/play.png)');
			}
			if(link.css('backgroundImage')=='none'){
				link.find('.hover_icon').css('backgroundImage','url(' +assetsUri+ '/link.png)');
			}
			link.css('backgroundImage','none');
		}
	});
	
	jQuery( '.portfolio_gallery.large_post_list' ).preloader({ imgSelector: '.portfolio_img_load span img', imgAppend: '.portfolio_img_load',
		oneachload: function(image){
			var img = jQuery(image),
				post = img.parent().parent().parent().parent(),
				link = img.parent().parent();
				
			img.removeClass('hover_fade_js');
			link.addClass('construct_hover_fade_js');
			post.find('.date').css('display','block');
			link.prepend('<span class=\"hover_overlay\"><span class=\"hover_icon\"></span></span>');
			if(link.css('backgroundImage').search('play.png')>0){
				link.find('.hover_icon').css('backgroundImage','url(' +assetsUri+ '/play.png)');
			}
			link.css('backgroundImage','none');
		}
	});
	
	jQuery( '.portfolio_gallery.medium_post_list' ).preloader({ imgSelector: '.portfolio_img_load span img', imgAppend: '.portfolio_img_load',
		oneachload: function(image){
			var img = jQuery(image),
				post = img.parent().parent().parent().parent(),
				link = img.parent().parent();
				
			img.removeClass('hover_fade_js');
			link.addClass('construct_hover_fade_js');
			post.find('.date').css('display','block');
			link.prepend('<span class=\"hover_overlay\"><span class=\"hover_icon\"></span></span>');
			if(link.css('backgroundImage').search('play.png')>0){
				link.find('.hover_icon').css('backgroundImage','url(' +assetsUri+ '/play.png)');
			}
			link.css('backgroundImage','none');
		}
	});
	
	jQuery( '.portfolio_gallery.small_post_list' ).preloader({ imgSelector: '.portfolio_img_load span img', imgAppend: '.portfolio_img_load',
		oneachload: function(image){
			var img = jQuery(image),
				post = img.parent().parent().parent().parent(),
				link = img.parent().parent();
				
			img.removeClass('hover_fade_js');
			link.addClass('construct_hover_fade_js');
			link.prepend('<span class=\"hover_overlay\"><span class=\"hover_icon\"></span></span>');
			if(link.css('backgroundImage').search('play.png')>0){
				link.find('.hover_icon').css('backgroundImage','url(' +assetsUri+ '/play.png)');
			}
			link.css('backgroundImage','none');
		}
	});
	
	jQuery( '#main_inner' ).preloader({ imgSelector: '.blog_sc_image_load span img', imgAppend: '.blog_sc_image_load',
		oneachload: function(image){
			var img = jQuery(image),
				link = img.parent().parent();
				
			img.removeClass('hover_fade_js');
			link.addClass('construct_hover_fade_js');
			link.prepend('<span class=\"hover_overlay\"><span class=\"hover_icon\"></span></span>');
			if(link.css('backgroundImage')=='none'){
				link.find('.hover_icon').css('backgroundImage','url(' +assetsUri+ '/link.png)');
			}
			link.css('backgroundImage','none');
			
		}
	});
	
	jQuery( '#main_inner, #sidebar_inner' ).preloader({ imgSelector: '.fancy_image_load span img', imgAppend: '.fancy_image_load',
		oneachload: function(image){
			var imageCaption = jQuery(image).parent().parent().next();
			if(imageCaption.length>0){
				imageCaption.remove();
				jQuery(image).parent().addClass('has_caption_frame');
				jQuery(image).parent().append(imageCaption);
				jQuery(image).next().css('display','block');
			}
			
			var img = jQuery(image),
				link = img.parent().parent();
				
			img.removeClass('hover_fade_js');
			link.css('backgroundImage','none');
			link.addClass('construct_hover_fade_js');
			link.prepend('<span class=\"hover_overlay\"><span class=\"hover_icon\"></span></span>');
		}
	});
	
	jQuery( '#intro_inner' ).preloader({ imgSelector: '.fancy_image_load span img', imgAppend: '.fancy_image_load',
		oneachload: function(image){
			var imageCaption = jQuery(image).parent().parent().next();
			if(imageCaption.length>0){
				imageCaption.remove();
				jQuery(image).parent().addClass('has_caption_frame');
				jQuery(image).parent().append(imageCaption);
				jQuery(image).next().css('display','block');
			}
			
			var img = jQuery(image),
				link = img.parent().parent();
				
			img.removeClass('hover_fade_js');
			link.css('backgroundImage','none');
			link.addClass('construct_hover_fade_js');
			link.prepend('<span class=\"hover_overlay\"><span class=\"hover_icon\"></span></span>');
		}
	});
	
	jQuery( '#footer_inner' ).preloader({ imgSelector: '.fancy_image_load span img', imgAppend: '.fancy_image_load',
		oneachload: function(image){
			var imageCaption = jQuery(image).parent().parent().next();
			if(imageCaption.length>0){
				imageCaption.remove();
				jQuery(image).parent().addClass('has_caption_frame');
				jQuery(image).parent().append(imageCaption);
				jQuery(image).next().css('display','block');
			}
			
			var img = jQuery(image),
				link = img.parent().parent();
				
			img.removeClass('hover_fade_js');
			link.css('backgroundImage','none');
			link.addClass('construct_hover_fade_js');
			link.prepend('<span class=\"hover_overlay\"><span class=\"hover_icon\"></span></span>');
		}
	});
	
	jQuery( '#main_inner' ).preloader({ imgSelector: '.portfolio_full_image span img', imgAppend: '.portfolio_full_image',
		oneachload: function(image){
			var img = jQuery(image),
				link = img.parent().parent();
				
			img.removeClass('hover_fade_js');
			link.addClass('construct_hover_fade_js');
			link.prepend('<span class=\"hover_overlay\"><span class=\"hover_icon\"></span></span>');
			if(link.css('backgroundImage').search('play.png')>0){
				link.find('.hover_icon').css('backgroundImage','url(' +assetsUri+ '/play.png)');
			}
			link.css('backgroundImage','none');
		}
 	});

	function mysite_jcarousel_setup(c) {
		c.clip.parent().parent().parent().parent().parent().removeClass('noscript');
		var jcarousel_img_load = c.clip.children().children().find('.post_grid_image .portfolio_img_load');
		
		if( jcarousel_img_load.length>1 ) {
			var link = jcarousel_img_load.parent().parent().children().find('.portfolio_img_load'),
				imgWidth = jcarousel_img_load.children().width(),
				imgHeight = jcarousel_img_load.children().height();
			
			jcarousel_img_load.css('width',imgWidth).css('height',imgHeight);
			jcarousel_img_load.children().removeClass('hover_fade_js');
			jcarousel_img_load.addClass('construct_hover_fade_js');
			jcarousel_img_load.prepend('<span class=\"hover_overlay\"><span class=\"hover_icon\"></span></span>');
			
			jcarousel_img_load.each(function(i) {
				var filename = jQuery(this).attr('href'),
					videos=['swf','youtube','vimeo','mov'];
					
				for(var v in videos){
				    if(filename.match(videos[v])){
						jcarousel_img_load.find('.hover_icon').css('backgroundImage','url(' +assetsUri+ '/play.png)');
					}
				}
			});
		}
	}
	
	/* ]]> */
	</script>";

	echo preg_replace( "/(\r\n|\r|\n)\s*/i", '', $out ) . "\r";

}
endif;

?>