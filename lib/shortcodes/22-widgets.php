<?php
/**
 *
 */
class mysiteWidgets {
	
	/**
	 *
	 */
	function testimonial( $atts = null ) {
		
		if( $atts == 'generator' ) {
			$numbers = range(1,20);
			foreach( $numbers as $val )
				$number[$val] = $val;
				
			$option = array( 
				'name' => __( 'Testimonial', 'backstop-themes-admin' ),
				'value' => 'testimonial',
				'options' => array(
					array(
						'name' => __( 'Count', 'backstop-themes-admin' ),
						'desc' => __( 'Select how many testimonials you want to be displayed.', 'backstop-themes-admin' ),
						'id' => 'number',
						'default' => '',
						'options' => $number,
						'type' => 'select'
					),
					array(
						'name' => __('Testimonials Categories <small>(optional)</small>', 'backstop-themes-admin' ),
						'desc' => __( 'If you want testimonials from specific categories to display then you may choose them here.', 'backstop-themes-admin' ),
						'id' => 'cat',
						'default' => array(),
						'target' => 'cat_testimonial',
						'type' => 'multidropdown'
					),
				'shortcode_has_atts' => true,
				)
			);

			return $option;
		}
		
		$defaults = array(
			'title' 			=> ' ',
			'number'			=> '4',
			'cat'				=> '',
			'testimonial_sc'	=> 'true',
		);
		
		$atts = wp_parse_args( $atts, $defaults );
		
		$instance = http_build_query( $atts );

		$args = array( 'widget_name' => 'MySite_Testimonial_Widget', 'instance' => $instance );
		
		$widget = new mysiteWidgets();
		return $widget->_widget_generator( $args );
	}

	/**
	 *
	 */
	function twitter( $atts = null ) {
		if( $atts == 'generator' ) {
			$numbers = range(1,20);
			foreach( $numbers as $val )
				$number[$val] = $val;

			$option = array( 
				'name' => __( 'Twitter', 'backstop-themes-admin' ),
				'value' => 'twitter',
				'options' => array(
					array(
						'name' => __( 'Username', 'backstop-themes-admin' ),
						'desc' => __( 'Paste your twitter username here.  You can find your username by going to your settings page within twitter.', 'backstop-themes-admin' ),
						'id' => 'id',
						'default' => '',
						'type' => 'text'
					),
					array(
						'name' => __( 'Count', 'backstop-themes-admin' ),
						'desc' => __( 'Select how many tweets you want to be displayed.', 'backstop-themes-admin' ),
						'id' => 'number',
						'default' => '',
						'options' => $number,
						'type' => 'select'
					),
				'shortcode_has_atts' => true,
				)
			);

			return $option;
		}
		
		$defaults = array(
			'id' 		=> '',
			'number'	=> '1',
			'title' 	=> ' '
		);
		
		if( isset( $atts['count'] ) )
			$atts['number'] = $atts['count'];
		
		if( isset( $atts['username'] ) )
			$atts['id'] = $atts['username'];
			
		if( empty( $atts['id'] ) )
			$atts['id'] = mysite_get_setting( 'twitter_id' );
			
		$atts = wp_parse_args( $atts, $defaults );
		
		$instance = http_build_query( $atts );

		$args = array( 'widget_name' => 'MySite_Twitter_Widget', 'instance' => $instance );
		
		$widget = new mysiteWidgets();
		return $widget->_widget_generator( $args );
	}
	
	/**
	 *
	 */
	function flickr( $atts = null ) {
		if( $atts == 'generator' ) {
			$numbers = range(1,20);
			foreach( $numbers as $val )
				$number[$val] = $val;

			$option = array( 
				'name' => __( 'Flickr', 'backstop-themes-admin' ),
				'value' => 'flickr',
				'options' => array(
					array(
						'name' => __( 'Flickr id (<a target="_blank" href="http://idgettr.com/">idGettr</a>)', 'backstop-themes-admin' ),
						'desc' => __( 'Set your Flickr ID here.  You can use the idGettr service to easily find your ID.', 'backstop-themes-admin' ),
						'id' => 'id',
						'default' => '',
						'type' => 'text'
					),
					array(
						'name' => __( 'Count', 'backstop-themes-admin' ),
						'desc' => __( 'Select how many flickr images you wish to display.', 'backstop-themes-admin' ),
						'id' => 'count',
						'default' => '',
						'options' => $number,
						'type' => 'select'
					),
					array(
						'name' => __( 'Size', 'backstop-themes-admin' ),
						'desc' => __( 'Set the size of your flickr images.<br /><br />Each setting will display differently so try and experiment with them to find which one suits you best.', 'backstop-themes-admin' ),
						'id' => 'size',
						'default' => '',
						'options' => array(
							's' => __('Square', 'backstop-themes-admin' ),
							't' => __('Thumbnail', 'backstop-themes-admin' ),
							'm' => __('Medium', 'backstop-themes-admin' )
						),
						'type' => 'select'
					),
					array(
						'name' => __('Display', 'backstop-themes-admin' ),
						'desc' => __( 'Select whether you want your latest images to display or a random selection.', 'backstop-themes-admin' ),
						'id' => 'display',
						'default' => '',
						'options' => array(
							'latest' => __('Latest', 'backstop-themes-admin' ),
							'random' => __('Random', 'backstop-themes-admin' )
						),
						'type' => 'select'
					),
				'shortcode_has_atts' => true,
				)
			);

			return $option;
		}
		
		$defaults = array(
			'id' 		=> '44071822@N08',
			'number'	=> '9',
			'display'	=> 'latest',
			'size'		=> 's',
			'title' 	=> ' '
		);
		
		$atts = wp_parse_args( $atts, $defaults );
		
		$instance = http_build_query( $atts );

		$args = array( 'widget_name' => 'MySite_Flickr_Widget', 'instance' => $instance );
		
		$widget = new mysiteWidgets();
		return $widget->_widget_generator( $args );
	}
	
	/**
	 *
	 */
	function recent_posts( $atts = null ) {
		if( $atts == 'generator' ) {
			$numbers = range(1,20);
			foreach( $numbers as $val )
				$number[$val] = $val;

			$option = array( 
				'name' => __( 'Recent Posts', 'backstop-themes-admin' ),
				'value' => 'recent_posts',
				'options' => array(
					array(
						'name' => __( 'Number', 'backstop-themes-admin' ),
						'desc' => __( 'Select the number of posts you wish to display.', 'backstop-themes-admin' ),
						'id' => 'number',
						'default' => '',
						'options' => $number,
						'type' => 'select'
					),
					array(
						'name' => __( 'Thumbnail', 'backstop-themes-admin' ),
						'desc' => __( 'Choose whether you want thumbnails to display alongside your posts.  The thumbnail uses the featured image of your post.', 'backstop-themes-admin' ),
						'id' => 'disable_thumb',
						'default' => '',
						'options' => array(
							'0' => __( 'Yes', 'backstop-themes-admin' ),
							'1' => __( 'No', 'backstop-themes-admin' )
						),
						'type' => 'select'
					),
				'shortcode_has_atts' => true,
				)
			);

		return $option;
		
		}
		
		$defaults = array(
			'number'	=> '',
			'disable_thumb'	=> '',
			'title' 	=> ' '
		);
		
		$atts = wp_parse_args( $atts, $defaults );
		
		$instance = http_build_query( $atts );

		$args = array( 'widget_name' => 'MySite_RecentPost_Widget', 'instance' => $instance );
		
		$widget = new mysiteWidgets();
		return $widget->_widget_generator( $args );
	}
	
	/**
	 *
	 */
	function popular_posts( $atts = null ) {
		if( $atts == 'generator' ) {
			$numbers = range(1,20);
			foreach( $numbers as $val )
				$number[$val] = $val;

			$option = array( 
				'name' => __( 'Popular Posts', 'backstop-themes-admin' ),
				'value' => 'popular_posts',
				'options' => array(
					array(
						'name' => __( 'Number', 'backstop-themes-admin' ),
						'desc' => __( 'Select the number of posts you wish to display.', 'backstop-themes-admin' ),
						'id' => 'number',
						'default' => '',
						'options' => $number,
						'type' => 'select'
					),
					array(
						'name' => __( 'Thumbnail', 'backstop-themes-admin' ),
						'desc' => __( 'Choose whether you want thumbnails to display alongside your posts.  The thumbnail uses the featured image of your post.', 'backstop-themes-admin' ),
						'id' => 'disable_thumb',
						'default' => '',
						'options' => array(
							'0' => __( 'Yes', 'backstop-themes-admin' ),
							'1' => __( 'No', 'backstop-themes-admin' )
						),
						'type' => 'select'
					),
				'shortcode_has_atts' => true,
				)
			);

		return $option;
		
		}
		
		$defaults = array(
			'number'	=> '',
			'disable_thumb'	=> '',
			'title' 	=> ' '
		);
		
		$atts = wp_parse_args( $atts, $defaults );
		
		$instance = http_build_query( $atts );

		$args = array( 'widget_name' => 'MySite_PopularPost_Widget', 'instance' => $instance );
		
		$widget = new mysiteWidgets();
		return $widget->_widget_generator( $args );
	}
	
	/**
	 *
	 */
	function contact_info( $atts = null ) {
		if( $atts == 'generator' ) {
			$option = array( 
				'name' => __( 'Contact Info', 'backstop-themes-admin' ),
				'value' => 'contact_info',
				'options' => array(
					array(
						'name' => __( 'Name', 'backstop-themes-admin' ),
						'desc' => __( 'Type in your name.', 'backstop-themes-admin' ),
						'id' => 'name',
						'default' => '',
						'type' => 'text'
					),
					array(
						'name' => __( 'Phone', 'backstop-themes-admin' ),
						'desc' => __( 'Type in your phone number.', 'backstop-themes-admin' ),
						'id' => 'phone',
						'default' => '',
						'type' => 'text'
					),
					array(
						'name' => __( 'Email', 'backstop-themes-admin' ),
						'desc' => __( 'Type in your email address.', 'backstop-themes-admin' ),
						'id' => 'email',
						'default' => '',
						'type' => 'text'
					),
					array(
						'name' => __( 'Address', 'backstop-themes-admin' ),
						'desc' => __( 'Type in your address.', 'backstop-themes-admin' ),
						'id' => 'address',
						'default' => '',
						'type' => 'text'
					),
					array(
						'name' => __( 'City', 'backstop-themes-admin' ),
						'desc' => __( 'Type in your city.', 'backstop-themes-admin' ),
						'id' => 'city',
						'default' => '',
						'type' => 'text'
					),
					array(
						'name' => __( 'State', 'backstop-themes-admin' ),
						'desc' => __( 'Type in your state.', 'backstop-themes-admin' ),
						'id' => 'state',
						'default' => '',
						'type' => 'text'
					),
					array(
						'name' => __( 'Zip', 'backstop-themes-admin' ),
						'desc' => __( 'Type in your zip.', 'backstop-themes-admin' ),
						'id' => 'zip',
						'default' => '',
						'type' => 'text'
					),
				'shortcode_has_atts' => true,
				)
			);

			return $option;
		}
		
		$defaults = array(
			'name'          => '',
			'address' 	=> '',
			'city'          => '',
			'state'         => '',
			'zip'           => '',
			'phone'         => '',
			'email'         => '',
			'title'         => ' '
		);
		
		$atts = wp_parse_args( $atts, $defaults );
		
		$instance = http_build_query( $atts );

		$args = array( 'widget_name' => 'MySite_Contact_Widget', 'instance' => $instance );
		
		$widget = new mysiteWidgets();
		return $widget->_widget_generator( $args );
	}
	
	/**
	 *
	 */
	function comments( $atts = null ) {
		if( $atts == 'generator' ) {
			$numbers = range(1,20);
			foreach( $numbers as $val )
				$number[$val] = $val;

			$option = array( 
				'name' => __( 'Comments', 'backstop-themes-admin' ),
				'value' => 'comments',
				'options' => array(
					array(
						'name' => __( 'Number', 'backstop-themes-admin' ),
						'desc' => __( 'Select the number of comments you wish to display.', 'backstop-themes-admin' ),
						'id' => 'number',
						'default' => '',
						'options' => $number,
						'type' => 'select'
					),
				'shortcode_has_atts' => true,
				)
			);

			return $option;
		}
			
		$defaults = array(
			'title' => ' ',
			'number' => '5'
		);
		
		$atts = wp_parse_args( $atts, $defaults );
		
		$instance = http_build_query( $atts );

		$args = array( 'widget_name' => 'WP_Widget_Recent_Comments', 'instance' => $instance );
		
		$widget = new mysiteWidgets();
		return $widget->_widget_generator( $args );
	}
	
	/**
	 *
	 */
	function tags( $atts = null ) {
		if( $atts == 'generator' ) {
			$option = array( 
				'name' => __( 'Tags', 'backstop-themes-admin' ),
				'value' => 'tags',
				'options' => array(
					array(
						'name' => __( 'Taxonomy', 'backstop-themes-admin' ),
						'desc' => __( 'Select whether you wish to display categories or tags.', 'backstop-themes-admin' ),
						'id' => 'taxonomy',
						'default' => '',
						'options' => array(
							'post_tag' => __( 'Post Tags', 'backstop-themes-admin' ),
							'category' => __( 'Category', 'backstop-themes-admin' )
						),
						'type' => 'select'
					),
				'shortcode_has_atts' => true,
				)
			);

			return $option;
		}
		
		$defaults = array(
			'title' => ' ',
			'taxonomy' => 'post_tag'
		);
			
		$atts = wp_parse_args( $atts, $defaults );
		
		$instance = http_build_query( $atts );

		$args = array( 'widget_name' => 'WP_Widget_Tag_Cloud', 'instance' => $instance );
		
		$widget = new mysiteWidgets();
		return $widget->_widget_generator( $args );
	}

	/**
	 *
	 */
	function rss( $atts = null ) {
		if( $atts == 'generator' ) {
			$numbers = range(1,20);
			foreach( $numbers as $val ) {
				$number[$val] = $val;
			}

			$option = array( 
				'name' => __( 'Rss', 'backstop-themes-admin' ),
				'value' => 'rss',
				'options' => array(
					array(
						'name' => __( 'RSS feed URL', 'backstop-themes-admin' ),
						'desc' => __( 'Paste the URL to your feed.  For example if you are using feedburner then you would paste something like this,<br /><br />http://feeds.feedburner.com/username', 'backstop-themes-admin' ),
						'id' => 'url',
						'default' => '',
						'type' => 'text'
					),
					array(
						'name' => __( 'How many items would you like to display?', 'backstop-themes-admin' ),
						'desc' => __( 'Select the number of RSS items you wish to display.', 'backstop-themes-admin' ),
						'id' => 'items',
						'default' => '',
						'options' => $number,
						'type' => 'select'
					),
					array(
						'name' => __( 'Show Summary', 'backstop-themes-admin' ),
						'desc' => __( 'Check this if you wish to display a summary of the item.', 'backstop-themes-admin' ),
						'id' => 'show_summary',
						'options' => array( '1' => __( 'Show Summary', 'backstop-themes-admin' )),
						'default' => '',
						'type' => 'checkbox'
					),
					array(
						'name' => __( 'Show Author', 'backstop-themes-admin' ),
						'desc' => __( 'Check if you wish to display the author of the item.', 'backstop-themes-admin' ),
						'id' => 'show_author',
						'options' => array( '1' => __( 'Show Author', 'backstop-themes-admin' )),
						'default' => '',
						'type' => 'checkbox'
					),
					array(
						'name' => __( 'Show Date', 'backstop-themes-admin' ),
						'desc' => __( 'Check if you wish to display the date of the item.', 'backstop-themes-admin' ),
						'id' => 'show_date',
						'options' => array( '1' => __( 'Show Date', 'backstop-themes-admin' )),
						'default' => '',
						'type' => 'checkbox'
					),
				'shortcode_has_atts' => true,
				)
			);
			
			return $option;
		}
		
		$defaults = array(
			'title' => ' ',
			'url' => '',
			'items' => 3,
			'error' => false,
			'show_summary' => 0,
			'show_author' => 0,
			'show_date' => 0
		);
		
		$atts = wp_parse_args( $atts, $defaults );
		
		$instance = http_build_query( $atts );

		$args = array( 'widget_name' => 'WP_Widget_RSS', 'instance' => $instance );
		
		$widget = new mysiteWidgets();
		return $widget->_widget_generator( $args );
	}
	
	/**
	 *
	 */
	function search( $atts = null ) {
		
		if( $atts == 'generator' ) {
			$option = array( 
				'name' => __( 'Search', 'backstop-themes-admin' ),
				'value' => 'search'
			);

			return $option;
		}
		
		$defaults = array( 'title' => ' ' );
		
		$atts = wp_parse_args( $atts, $defaults );
		
		$instance = http_build_query( $atts );

		$args = array( 'widget_name' => 'WP_Widget_Search', 'instance' => $instance );
		
		$widget = new mysiteWidgets();
		return $widget->_widget_generator( $args );
	}

	/**
	 *
	 */
	function _widget_generator( $args = array() ) {
		global $wp_widget_factory;
		
		$widget_name = esc_html( $args['widget_name'] );

		ob_start();
		the_widget( $widget_name, $args['instance'], array( 'before_title' => '', 'after_title' => '', 'widget_id' => '-1' ) );
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}

	/**
	 *
	 */
	function _options( $class ) {
		$shortcode = array();
		
		$class_methods = get_class_methods( $class );
		
		foreach( $class_methods as $method ) {
			if( $method[0] != '_' ) {
				$shortcode[] = call_user_func(array( &$class, $method ), $atts = 'generator' );
			}
		}
		
		$options = array(
			'name' => __( 'Widget', 'backstop-themes-admin' ),
			'desc' => __( 'Select which widget shortcode you would like to use.', 'backstop-themes-admin' ),
			'value' => 'widget',
			'options' => $shortcode,
			'shortcode_has_types' => true
		);
		
		return $options;
	}
	
}

?>