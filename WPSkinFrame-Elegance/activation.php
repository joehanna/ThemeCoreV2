<?php
/**
 *
 */
function mysite_default_site_options() {
	$general_options_array = Array
	(
		"display_logo" => "",
		"logo_url" => "",
		"favicon_url" => "",
		"intro_options" => "title_teaser",
		"custom_teaser" => "This is the default teaser text option. You can remove or edit this text under your 'General Settings' tab. This can also be overwritten on a page by page basis.",
		"custom_teaser_html" => "",
		"page_layout" => "right_sidebar",
		"twitter_id" => "",
		"extra_header" => "",
		"disable_page_comments" => "true",
		"disable_cufon" => "",
		"disable_breadcrumbs" => "true",
		"breadcrumb_delimiter" => "/",
		"analytics_code" => "",
		"custom_css" => "",
		"custom_js" => "",
		"additional_headers" => "",
		"homepage_layout" => "full_width",
		"homepage_teaser_text" => "Put your Teaser text here!",
		"teaser_button_text" => "Get it Now!",
		"teaser_button" => "custom",
		"teaser_button_page" => "",
		"teaser_button_custom" => "//backstopthemes.com/",
		"content" => "[one_fourth]<br /><br /><h6>Phasellus commodo</h6><br /><br />[dropcap1]1[/dropcap1]Cras euismod rhoncus tincidunt. Aenean pulvinar pharetra est, in hendrerit erat condimentum non. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.[/one_fourth]<br /><br />[one_fourth]<br /><br /><h6>Phasellus commodo</h6><br /><br />[dropcap1]2[/dropcap1]Cras euismod rhoncus tincidunt. Aenean pulvinar pharetra est, in hendrerit erat condimentum non. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.[/one_fourth]<br /><br />[one_fourth]<br /><br /><h6>Phasellus commodo</h6><br /><br />[dropcap1]3[/dropcap1]Cras euismod rhoncus tincidunt. Aenean pulvinar pharetra est, in hendrerit erat condimentum non. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.[/one_fourth]<br /><br />[one_fourth_last]<br /><br /><h6>Phasellus commodo</h6><br /><br />[dropcap1]4[/dropcap1]Cras euismod rhoncus tincidunt. Aenean pulvinar pharetra est, in hendrerit erat condimentum non. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.[/one_fourth_last]",
		"mainpage_content" => "",
		"home_slider_disable" => "",
		"frontpage_blog" => "",
		"homepage_footer_teaser" => "twitter",
		"skin_generator" => "",
		"style_variations" => "",
		"image_resize" => "",
		"image_resize_type" => "",
		"auto_img" => "",
		"one_column_portfolio_full" => Array	(	"w" => "960",	"h" => "596"),
		"two_column_portfolio_full" => Array	(	"w" => "460",	"h" => "285"),
		"three_column_portfolio_full" => Array	(	"w" => "294",	"h" => "182"),
		"four_column_portfolio_full" => Array	(	"w" => "211",	"h" => "131"),
		"portfolio_single_full_full" => Array	(	"w" => "960",	"h" => "596"),
		"one_column_blog_full" => Array			(	"w" => "960",	"h" => "400"),
		"two_column_blog_full" => Array			(	"w" => "460",	"h" => "191"),
		"three_column_blog_full" => Array		(	"w" => "294",	"h" => "122"),
		"four_column_blog_full" => Array		(	"w" => "211",	"h" => "87"	),
		"small_post_list_full" => Array			(	"w" => "50"	,	"h" => "50"	),
		"medium_post_list_full" => Array		(	"w" => "200",	"h" => "200"),
		"large_post_list_full" => Array			(	"w" => "627",	"h" => "389"),
		"additional_posts_grid_full" => Array	(	"w" => "240",	"h" => "150"),
		"one_column_portfolio_big" => Array		(	"w" => "660",	"h" => "409"),
		"two_column_portfolio_big" => Array		(	"w" => "316",	"h" => "196"),
		"three_column_portfolio_big" => Array	(	"w" => "202",	"h" => "125"),
		"four_column_portfolio_big" => Array	(	"w" => "145",	"h" => "90"	),
		"portfolio_single_full_big" => Array	(	"w" => "660",	"h" => "409"),
		"one_column_blog_big" => Array			(	"w" => "660",	"h" => "275"),
		"two_column_blog_big" => Array			(	"w" => "316",	"h" => "131"),
		"three_column_blog_big" => Array		(	"w" => "202",	"h" => "84"	),
		"four_column_blog_big" => Array			(	"w" => "145",	"h" => "60"	),
		"small_post_list_big" => Array			(	"w" => "50"	,	"h" => "50"	),
		"medium_post_list_big" => Array			(	"w" => "200",	"h" => "200"),
		"large_post_list_big" => Array			(	"w" => "431",	"h" => "267"),
		"additional_posts_grid_big" => Array	(	"w" => "165",	"h" => "110"),
		"one_column_portfolio_small" => Array	(	"w" => "700",	"h" => "434"),
		"two_column_portfolio_small" => Array	(	"w" => "336",	"h" => "208"),
		"three_column_portfolio_small" => Array	(	"w" => "214",	"h" => "132"),
		"four_column_portfolio_small" => Array	(	"w" => "154",	"h" => "95"	),
		"portfolio_single_full_small" => Array	(	"w" => "700",	"h" => "434"),
		"one_column_blog_small" => Array		(	"w" => "700",	"h" => "291"),
		"two_column_blog_small" => Array		(	"w" => "336",	"h" => "140"),
		"three_column_blog_small" => Array		(	"w" => "214",	"h" => "89"	),
		"four_column_blog_small" => Array		(	"w" => "154",	"h" => "64"	),
		"small_post_list_small" => Array		(	"w" => "50"	,	"h" => "50"	),
		"medium_post_list_small" => Array		(	"w" => "200",	"h" => "200"),
		"large_post_list_small" => Array		(	"w" => "457",	"h" => "283"),
		"additional_posts_grid_small" => Array	(	"w" => "175",	"h" => "115"),
		"homepage_slider" => "fading_slider",
		"nivo_effect" => "",
		"nivo_slices" => "",
		"nivo_anim_speed" => "",
		"static_slider_content" => "",
		"static_slider_content_text" => "",
		"responsive_effect" => "",
		"responsive_direction" => "",
		"responsive_anim_speed" => "1000",
		"slider_speed" => "4000",
		"slider_disable_trans" => "",
		"slider_hover_pause" => "",
		"responsive_direction_nav" => "",
		"responsive_dots_nav" => "",
		"responsive_randomize" => "",
		"responsive_slider_transitions" => "",
		"slider_fade_speed" => "slow",
		"slider_nav" => "dots",
		"nivo_direction_nav" => "button_hover",
		"nivo_control_nav" => "",
		"slider_page" => "",
		"slider_custom" => "custom",
		"slider_cat_count" => "",
		"slideshow" => Array
			(
				"2" => Array("slider_url" => "%site_url%/partial_staged_slide.jpg",	"alt_attr" => "","link_url" => "//backstopthemes.com","title" => "Partial Staged Left",	"stage_effect" => "partial_staged_slideL",
					"description" => "Proin volutpat fermentum purus sed varius. Nullam interdum massa a libero mattis et accumsan diam pulvinar. Aenean dui est, feugiat nec sagittis ut, lobortis non tellus. Nam id erat a lacus sagittis tempor"),
				"1" => Array("slider_url" => "%site_url%/staged.jpg","alt_attr" => "",	"link_url" => "","title" => "",	"stage_effect" => "staged_slide","description" => ""),
				"3" => Array("slider_url" => "%site_url%/partial_gradient_slide.jpg","alt_attr" => "",	"link_url" => "//backstopthemes.com",	"title" => "Partial Gradient Slide","stage_effect" => "partial_gradient_slide",	
					"description" => "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum faucibus consequat tellus non vestibulum. Phasellus tortor mi, faucibus sit amet ullamcorper nec"),
				"4" => Array("slider_url" => "%site_url%/floating.jpg",	"alt_attr" => "","link_url" => "",	"title" => "",	"stage_effect" => "floating_slide",	"description" => ""	),
				"5" => Array("slider_url" => "%site_url%/full.jpg",	"alt_attr" => "","link_url" => "",	"title" => "",	"stage_effect" => "full_slide",	"description" => ""	),
				"#" => Array("slider_url" => "","alt_attr" => "","link_url" => "","title" => "","stage_effect" => "staged_slide",	"description" => ""	),
				"slider_keys" => "2,1,3,4,5,#"
			),
		"slider_cats" => "",
		"blog_page" => "",
		"blog_layout" => "blog_layout1",
		"post_layout" => "right_sidebar",
		"exclude_categories" => "",
		"post_like_module" => "tab",
		"post_comment_styles" => "tab",
		"display_full" => "",
		"disable_post_author" => "",
		"disable_post_nav" => "true",
		"social_bookmarks" => "",
		"url_shortening" => "",
		"bitly_login" => "",
		"bitly_api" => "",
		"disable_meta_options" => "",
		"custom_sidebars" => "",
		"footer_columns" => "sixth_sixth_sixth_sixth_third",
		"footer_disable" => "",
		"footer_teaser" => "twitter",
		"footer_text" => "&copy; <a href='//backstopthemes.com'>backstopthemes.com</a>",
		"sociable" => Array
			(
				"2" => Array("icon" => "twitter.png",	"color" => "default",	"custom" => "",	"link" => "#",	"alt" => ""	),
				"3" => Array("icon" => "rss.png",	"color" => "default",	"custom" => "",	"link" => "#",	"alt" => ""	),
				"4" => Array("icon" => "email.png","color" => "default","custom" => "",	"link" => "#",	"alt" => ""	),
				"5" => Array("icon" => "skype.png","color" => "default","custom" => "",	"link" => "#",	"alt" => ""	),
				"#" => Array("icon" => "delicious.png","color" => "black_circles","custom" => "","link" => "",	"alt" => ""	),
				"keys" => "2,3,4,5,#"
			),
		"seo_home_title" => "",
		"seo_home_description" => "",
		"seo_home_keywords" => "",
		"seo_can" => "",
		"seo_post_title_format" => "",
		"seo_page_title_format" => "",
		"seo_category_title_format" => "",
		"seo_archive_title_format" => "",
		"seo_tag_title_format" => "",
		"seo_search_title_format" => "",
		"seo_description_format" => "",
		"seo_404_title_format" => "",
		"seo_paged_format" => "",
		"seo_enablecpost" => "",
		"seo_posttypecolumns" => Array				(	"0" => "post",	"1" => "page",	"2" => "portfolio"	),
		"seo_use_categories" => "",
		"seo_use_tags_as_keywords" => "",
		"seo_dynamic_postspage_keywords" => "",
		"seo_category_noindex" => "",
		"seo_archive_noindex" => "",
		"seo_tags_noindex" => "",
		"seo_generate_descriptions" => "",
		"seo_cap_cats" => "",
		"seo_ex_pages" => "",
		"seo_post_meta_tags" => "",
		"seo_page_meta_tags" => "",
		"seo_home_meta_tags" => "",
		"archive_layout" => "",
		"archive_custom_sidebar" => "",
		"archive_custom_background" => Array(	"url" => "","background-color" => "",	"background-repeat" => "repeat",	"background-attachment" => "scroll",	"background-position" => "left top"	),
		"search_layout" => "",
		"search_custom_sidebar" => "",
		"search_custom_background" => Array	(	"url" => "", "background-color" => "",	"background-repeat" => "repeat",	"background-attachment" => "scroll",	"background-position" => "left top"	),
		"four_04_layout" => "",
		"four_04_custom_sidebar" => "",
		"four_04_custom_background" => Array(	"url" => "","background-color" => "",	"background-repeat" => "repeat",	"background-attachment" => "scroll",	"background-position" => "left top"	),
		"responsive_options" => "mobile",
		"mobile_slider" => "default_slider",
		"mobile_slider_custom" => "",
		"mobile_disable_shortcodes" => Array("galleria" => "",	"slider" => "","tooltips" => "","jcarousel" => ""	),
		"mobile_custom_css" => "",
		"mobile_custom_js" => "",
		"custom_user_agents" => "",
		"admin_logo_url" => "",
		"import_options" => "",
		"export_options" => "1"
	);

	return $general_options_array;
}

?>