<?php
/**
 *
 */
class mysiteSocial {

	/**
	 *  ReTweet button
	 */
	function tweet( $atts = null, $content = null ) {
	
		if( $atts == 'generator' ) {
			$option = array( 
				"name" => __( "Twitter", 'backstop-themes-admin' ),
				"value" => "tweet",
				"options" => array(
					array(
						"name" => __( "Twitter Username", 'backstop-themes-admin' ),
						"id" => "username",
						"desc" => __( 'Type out your twitter username here.  You can find your twitter username by logging into your twitter account.', 'backstop-themes-admin' ),
						"default" => "",
						"type" => "text"
					),
					array(
						"name" => __( "Tweet Position", 'backstop-themes-admin' ),
						"id" => "layout",
						"desc" => __( 'Choose whether you want your tweets to display vertically, horizontally, or none at all.', 'backstop-themes-admin' ),
						"default" => "",
						"options" => array(
							"vertical" => __( "Vertical", 'backstop-themes-admin' ),
							"horizontal" => __( "Horizontal", 'backstop-themes-admin' ),
							"none" => __( "None", 'backstop-themes-admin' ),
						),
						"type" => "select"
					),
					array(
						"name" => __( "Custom Text", 'backstop-themes-admin' ),
						"id" => "text",
						"desc" => __( 'This is the text that people will include in their Tweet when they share from your website.', 'backstop-themes-admin' ),
						"default" => "",
						"type" => "text"
					),
					array(
						"name" => __( "Custom URL", 'backstop-themes-admin' ),
						"id" => "url",
						"desc" => __( 'By default the URL from your page will be used but you can input a custom URL here.', 'backstop-themes-admin' ),
						"default" => "",
						"type" => "text"
					),
					array(
						"name" => __( "Related Users", 'backstop-themes-admin' ),
						"id" => "related",
						"desc" => __( 'You can input another twitter username for recommendation.', 'backstop-themes-admin' ),
						"default" => "",
						"type" => "text"
					),
					array(
						"name" => __( "Language", 'backstop-themes-admin' ),
						"id" => "lang",
						"desc" => __( 'Select which language you would like to display the button in.', 'backstop-themes-admin' ),
						"default" => "",
						"options" => array(
							"fr" => __( "French", 'backstop-themes-admin' ),
							"de" => __( "German", 'backstop-themes-admin' ),
							"it" => __( "Italian", 'backstop-themes-admin' ),
							"ja" => __( "Japanese", 'backstop-themes-admin' ),
							"ko" => __( "Korean", 'backstop-themes-admin' ),
							"ru" => __( "Russian", 'backstop-themes-admin' ),
							"es" => __( "Spanish", 'backstop-themes-admin' ),
							"tr" => __( "Turkish", 'backstop-themes-admin' ),
						),
						"type" => "select"
					),
					"shortcode_has_atts" => true,
				)
			);

			return $option;
		}
		
		extract(shortcode_atts(array(
			'layout'        => 'vertical',
			'username'		  => '',
			'text' 			  => '',
			'url'			  => '',
			'related'		  => '',
			'lang'			  => '',
	    	), $atts));
	
		if( is_feed() ) return;
	    	
	    if ($text != '') { $text = "data-text='".$text."'"; }
	    if ($url != '') { $url = "data-url='".$url."'"; }
	    if ($related != '') { $related = "data-related='".$related."'"; }
	    if ($lang != '') { $lang = "data-lang='".$lang."'"; }
		
		$out = '<div class = "mysite_sociable"><a href="http://twitter.com/share" class="twitter-share-button" '.$url.' '.$lang.' '.$text.' '.$related.' data-count="'.$layout.'" data-via="'.$username.'">Tweet</a>';
		$out .= '<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script></div>';
		
		return $out;
	}
	
	/**
	 *  Facebook Like button
	 */
	function fblike( $atts = null, $content = null ) {

	    if( $atts == 'generator' ) {
	        $option = array(
	            "name" => __( "Facebook Like", 'backstop-themes-admin' ),
	            "value" => "fblike",
	            "options" => array(
	                array(
	                    "name" => __( "Layout", 'backstop-themes-admin' ),
	                    "id" => "layout",
	                    "desc" => __( 'Choose the layout you would like to use with your facebook button.', 'backstop-themes-admin' ),
	                    "default" => "",
	                    "options" => array(
	                        "standard" => __( "Standard", 'backstop-themes-admin' ),
	                        "box_count" => __( "Box Count", 'backstop-themes-admin' ),
	                        "button_count" => __( "Button Count", 'backstop-themes-admin' ),
	                    ),
	                    "type" => "select"
	                ),
	                array(
	                    "name" => __( "Add send button?", 'backstop-themes-admin' ),
	                    "id" => "send",
	                    "desc" => __( 'Check to add the send button alongside the like button.', 'backstop-themes-admin' ),
	                    'options' => array( 'true' => __( 'Yes', 'backstop-themes-admin' )),
	                    'default' => '',
	                    'type' => 'checkbox'
	                ),
	                array(
	                    "name" => __( "Show Faces?", 'backstop-themes-admin' ),
	                    "id" => "show_faces",
	                    "desc" => __( 'Check to display faces.', 'backstop-themes-admin' ),
	                    'options' => array( 'true' => __( 'Yes', 'backstop-themes-admin' )),
	                    'default' => '',
	                    'type' => 'checkbox'
	                ),
	                array(
	                    "name" => __( "Action", 'backstop-themes-admin' ),
	                    "id" => "action",
	                    "desc" => __( 'This is the text that gets displayed on the button.', 'backstop-themes-admin' ),
	                    "default" => "",
	                    "options" => array(
	                        "like" => __( "Like", 'backstop-themes-admin' ),
	                        "recommend" => __( "Recommend", 'backstop-themes-admin' ),
	                    ),
	                    "type" => "select"
	                ),
	                array(
	                    "name" => __( "Font", 'backstop-themes-admin' ),
	                    "id" => "font",
	                    "desc" => __( 'Select which font you would like to use.', 'backstop-themes-admin' ),
	                    "default" => "",
	                    "options" => array(
	                        "lucida+grande" => __( "Lucida Grande", 'backstop-themes-admin' ),
	                        "arial" => __( "Arial", 'backstop-themes-admin' ),
	                        "segoe+ui" => __( "Segoe Ui", 'backstop-themes-admin' ),
	                        "tahoma" => __( "Tahoma", 'backstop-themes-admin' ),
	                        "trebuchet+ms" => __( "Trebuchet MS", 'backstop-themes-admin' ),
	                        "verdana" => __( "Verdana", 'backstop-themes-admin' ),
	                    ),
	                    "type" => "select"
	                ),
	                array(
	                    "name" => __( "Color Scheme", 'backstop-themes-admin' ),
	                    "id" => "colorscheme",
	                    "desc" => __( 'Select the color scheme you would like to use.', 'backstop-themes-admin' ),
	                    "default" => "",
	                    "options" => array(
	                        "light" => __( "Light", 'backstop-themes-admin' ),
	                        "dark" => __( "Dark", 'backstop-themes-admin' ),
	                    ),
	                    "type" => "select"
	                ),
	                "shortcode_has_atts" => true,
	            )
	        );

	        return $option;
	    }

	    extract(shortcode_atts(array(
	            'layout'        => 'box_count',
	            'width'            => '',
	            'height'        => '',
	            'send'            => false,
	            'show_faces'    => false,
	            'action'        => 'like',
	            'font'            => 'lucida+grande',
	            'colorscheme'    => 'light',
	        ), $atts));

	    if( is_feed() ) return;

	    if ($layout == 'standard') { $width = '450'; $height = '35';  if ($show_faces == 'true') { $height = '80'; } }
	    if ($layout == 'box_count') { $width = '55'; $height = '65'; }
	    if ($layout == 'button_count') { $width = '90'; $height = '20'; }

	    $layout = 'data-layout = "'.$layout.'"';
	    $width = 'data-width = "'.$width.'"';
	    $font = 'data-font = "'.str_replace("+", " ", $font).'"';
	    $colorscheme = 'data-colorscheme = "'.$colorscheme.'"';
	    $action = 'data-action = "'.$action.'"';
	    if ( $show_faces ) { $show_faces = 'data-show-faces = "true"'; } else { $show_faces = ''; }
	    if ( $send ) { $send = 'data-send = "true"'; } else { $send = ''; }

	    $out = '<div class = "mysite_sociable">';
	    $out .= '<div id="fb-root"></div><script>(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;js = d.createElement(s); js.id = id;js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";fjs.parentNode.insertBefore(js, fjs);}(document, "script", "facebook-jssdk"));</script>';
	    $out .= '<div class = "fb-like" data-href = "'.get_permalink().'" '.$layout.$width.$font.$colorscheme.$action.$show_faces.$send.'></div>';
	    $out .= '</div>';

	    return $out;
	}
	
	
	/**
	 *  Google +1
	 */
	function googleplusone( $atts = null, $content = null ) {
	
		if( $atts == 'generator' ) {
			$option = array( 
				"name" => __( "Google +1", 'backstop-themes-admin' ),
				"value" => "googleplusone",
				"options" => array(
					array(
						"name" => __( "Size", 'backstop-themes-admin' ),
						"id" => "size",
						"desc" => __( 'Choose how you would like to display the google plus button.', 'backstop-themes-admin' ),
						"default" => "",
						"options" => array(
							"small" => __( "Small", 'backstop-themes-admin' ),
							"standard" => __( "Standard", 'backstop-themes-admin' ),
							"medium" => __( "Medium", 'backstop-themes-admin' ),
							"tall" => __( "Tall", 'backstop-themes-admin' ),
						),
						"type" => "select"
					),
					array(
						"name" => __( "Language", 'backstop-themes-admin' ),
						"id" => "lang",
						"desc" => __( 'Select which language you would like to display the button in.', 'backstop-themes-admin' ),
						"default" => "",
						"options" => array(
							"ar" => __( "Arabic", 'backstop-themes-admin' ),
							"bn" => __( "Bengali", 'backstop-themes-admin' ),
							"bg" => __( "Bulgarian", 'backstop-themes-admin' ),
							"ca" => __( "Catalan", 'backstop-themes-admin' ),
							"zh" => __( "Chinese", 'backstop-themes-admin' ),
							"zh_CN" => __( "Chinese (China)", 'backstop-themes-admin' ),
							"zh_HK" => __( "Chinese (Hong Kong)", 'backstop-themes-admin' ),
							"zh_TW" => __( "Chinese (Taiwan)", 'backstop-themes-admin' ),
							"hr" => __( "Croation", 'backstop-themes-admin' ),
							"cs" => __( "Czech", 'backstop-themes-admin' ),
							"da" => __( "Danish", 'backstop-themes-admin' ),
							"nl" => __( "Dutch", 'backstop-themes-admin' ),
							"en_IN" => __( "English (India)", 'backstop-themes-admin' ),
							"en_IE" => __( "English (Ireland)", 'backstop-themes-admin' ),
							"en_SG" => __( "English (Singapore)", 'backstop-themes-admin' ),
							"en_ZA" => __( "English (South Africa)", 'backstop-themes-admin' ),
							"en_GB" => __( "English (United Kingdom)", 'backstop-themes-admin' ),
							"fil" => __( "Filipino", 'backstop-themes-admin' ),
							"fi" => __( "Finnish", 'backstop-themes-admin' ),
							"fr" => __( "French", 'backstop-themes-admin' ),
							"de" => __( "German", 'backstop-themes-admin' ),
							"de_CH" => __( "German (Switzerland)", 'backstop-themes-admin' ),
							"el" => __( "Greek", 'backstop-themes-admin' ),
							"gu" => __( "Gujarati", 'backstop-themes-admin' ),
							"iw" => __( "Hebrew", 'backstop-themes-admin' ),
							"hi" => __( "Hindi", 'backstop-themes-admin' ),
							"hu" => __( "Hungarian", 'backstop-themes-admin' ),
							"in" => __( "Indonesian", 'backstop-themes-admin' ),
							"it" => __( "Italian", 'backstop-themes-admin' ),
							"ja" => __( "Japanese", 'backstop-themes-admin' ),
							"kn" => __( "Kannada", 'backstop-themes-admin' ),
							"ko" => __( "Korean", 'backstop-themes-admin' ),
							"lv" => __( "Latvian", 'backstop-themes-admin' ),
							"ln" => __( "Lingala", 'backstop-themes-admin' ),
							"lt" => __( "Lithuanian", 'backstop-themes-admin' ),
							"ms" => __( "Malay", 'backstop-themes-admin' ),
							"ml" => __( "Malayalam", 'backstop-themes-admin' ),
							"mr" => __( "Marathi", 'backstop-themes-admin' ),
							"no" => __( "Norwegian", 'backstop-themes-admin' ),
							"or" => __( "Oriya", 'backstop-themes-admin' ),
							"fa" => __( "Persian", 'backstop-themes-admin' ),
							"pl" => __( "Polish", 'backstop-themes-admin' ),
							"pt_BR" => __( "Portugese (Brazil)", 'backstop-themes-admin' ),
							"pt_PT" => __( "Portugese (Portugal)", 'backstop-themes-admin' ),
							"ro" => __( "Romanian", 'backstop-themes-admin' ),
							"ru" => __( "Russian", 'backstop-themes-admin' ),
							"sr" => __( "Serbian", 'backstop-themes-admin' ),
							"sk" => __( "Slovak", 'backstop-themes-admin' ),
							"sl" => __( "Slovenian", 'backstop-themes-admin' ),
							"es" => __( "Spanish", 'backstop-themes-admin' ),
							"sv" => __( "Swedish", 'backstop-themes-admin' ),
							"gsw" => __( "Swiss German", 'backstop-themes-admin' ),
							"ta" => __( "Tamil", 'backstop-themes-admin' ),
							"te" => __( "Telugu", 'backstop-themes-admin' ),
							"th" => __( "Thai", 'backstop-themes-admin' ),
							"tr" => __( "Turkish", 'backstop-themes-admin' ),
							"uk" => __( "Ukranian", 'backstop-themes-admin' ),
							"vi" => __( "Vietnamese", 'backstop-themes-admin' ),
						),
						"type" => "select"
					),
					"shortcode_has_atts" => true,
				)
			);

			return $option;
		}
		
		extract(shortcode_atts(array(
				'size'			=> '',
				'lang'			=> '',
	    ), $atts));
		
		if( is_feed() ) return;
		
	    if ($size != '') { $size = "size='".$size."'"; }
	    if ($lang != '') { $lang = "{lang: '".$lang."'}"; }
	    
		$out = '<div class = "mysite_sociable"><script type="text/javascript" src="https://apis.google.com/js/plusone.js">'.$lang.'</script>';
		$out .= '<g:plusone '.$size.'></g:plusone></div>';
	    		
		return $out;
	}
	
	/**
	 *  Digg button
	 */
	function digg( $atts = null, $content = null ) {
	
		if( $atts == 'generator' ) {
			$option = array( 
				"name" => __( "Digg", 'backstop-themes-admin' ),
				"value" => "digg",
				"options" => array(
					array(
						"name" => __( "Layout", 'backstop-themes-admin' ),
						"id" => "layout",
						"desc" => __( 'Choose how you would like to display the digg button.', 'backstop-themes-admin' ),
						"default" => "",
						"options" => array(
							"DiggWide" => __( "Wide", 'backstop-themes-admin' ),
							"DiggMedium" => __( "Medium", 'backstop-themes-admin' ),
							"DiggCompact" => __( "Compact", 'backstop-themes-admin' ),
							"DiggIcon" => __( "Icon", 'backstop-themes-admin' ),
						),
						"type" => "select"
					),
					array(
						"name" => __( "Custom URL", 'backstop-themes-admin' ),
						"id" => "url",
						"desc" => __( 'In case you wish to use a different URL you can input it here.', 'backstop-themes-admin' ),
						"default" => "",
						"type" => "text"
					),
					array(
						"name" => __( "Custom Title", 'backstop-themes-admin' ),
						"id" => "title",
						"desc" => __( 'In case you wish to use a different title you can input it here.', 'backstop-themes-admin' ),
						"default" => "",
						"type" => "text"
					),
					array(
						"name" => __( "Article Type", 'backstop-themes-admin' ),
						"id" => "type",
						"desc" => __( 'You can set the article type here for digg.<br /><br />For example if you wanted to set it in the gaming or entertainment topics then you would type this, "gaming, entertainment".', 'backstop-themes-admin' ),
						"default" => "",
						"type" => "text"
					),
					array(
						"name" => __( "Custom Description", 'backstop-themes-admin' ),
						"id" => "description",
						"desc" => __( 'You can set a custom description to be displayed within digg here.', 'backstop-themes-admin' ),
						"default" => "",
						"type" => "text"
					),
					array(
						"name" => __( "Related Stories", 'backstop-themes-admin' ),
						"id" => "related",
						"desc" => __( 'This option allows you to specify whether links to related stories should be present in the pop up window that may appear when users click the button.', 'backstop-themes-admin' ),
						"default" => "",
						"options" => array(
							"true" => __( "Disable related stories?", 'backstop-themes-admin' ),
						),
						"type" => "checkbox"
					),
					"shortcode_has_atts" => true,
				)
			);			
			
			return $option;
		}
				
		extract(shortcode_atts(array(
			'layout'        => 'DiggMedium',
			'url'			=> get_permalink(),
			'title'			=> '',
			'type'			=> '',
			'description'	=> '',
			'related'		=> '',
	    	), $atts));
	
		if( is_feed() ) return;
	    
	    if ($title != '') { $title = "&title='".$title."'"; }
	    if ($type != '') { $type = "rev='".$type."'"; }
	    if ($description != '') { $description = "<span style = 'display: none;'>".$description."</span>"; }
	    if ($related != '') { $related = "&related=no"; }
	    	
		$out = '<div class = "mysite_sociable"><a class="DiggThisButton '.$layout.'" href="http://digg.com/submit?url='.$url.$title.$related.'"'.$type.'>'.$description.'</a>';
		$out .= '<script type = "text/javascript" src = "http://widgets.digg.com/buttons.js"></script></div>';
		
		return $out;
	}
	
	/**
	 *  Stumbleupon button
	 */
	function stumbleupon( $atts = null, $content = null ) {
	
		if( $atts == 'generator' ) {
			$option = array( 
				"name" => __( "Stumbleupon", 'backstop-themes-admin' ),
				"value" => "stumbleupon",
				"options" => array(
					array(
						"name" => __( "Layout", 'backstop-themes-admin' ),
						"id" => "layout",
						"desc" => __( 'Choose how you would like to display the stumbleupon button.', 'backstop-themes-admin' ),
						"default" => "",
						"options" => array(
							"1" => __( "Style 1", 'backstop-themes-admin' ),
							"2" => __( "Style 2", 'backstop-themes-admin' ),
							"3" => __( "Style 3", 'backstop-themes-admin' ),
							"4" => __( "Style 4", 'backstop-themes-admin' ),
							"5" => __( "Style 5", 'backstop-themes-admin' ),
							"6" => __( "Style 6", 'backstop-themes-admin' ),
						),
						"type" => "select"
					),
					array(
						"name" => __( "Custom URL", 'backstop-themes-admin' ),
						"id" => "url",
						"desc" => __( 'You can set a custom URL to be displayed within stumbleupon here.', 'backstop-themes-admin' ),
						"default" => "",
						"type" => "text"
					),
					"shortcode_has_atts" => true,
				)
			);			
			
			return $option;
		}
		
		extract(shortcode_atts(array(
			'layout'        => '5',
			'url'			=> '',
	    	), $atts));
	
		if( is_feed() ) return;
	    	
	    if ($url != '') { $url = "&r=".$url; }
	    	
		return '<div class = "mysite_sociable"><script src="http://www.stumbleupon.com/hostedbadge.php?s='.$layout.$url.'"></script></div>';
	}
	
	/**
	 *  Reddit button
	 */
	function reddit( $atts = null, $content = null ) {
	
		if( $atts == 'generator' ) {
			$option = array( 
				"name" => __( "Reddit", 'backstop-themes-admin' ),
				"value" => "reddit",
				"options" => array(
					array(
						"name" => __( "Layout", 'backstop-themes-admin' ),
						"id" => "layout",
						"desc" => __( 'Choose how you would like to display the reddit button.', 'backstop-themes-admin' ),
						"default" => "",
						"options" => array(
							"1" => __( "Style 1", 'backstop-themes-admin' ),
							"2" => __( "Style 2", 'backstop-themes-admin' ),
							"3" => __( "Style 3", 'backstop-themes-admin' ),
							"4" => __( "Style 4", 'backstop-themes-admin' ),
							"5" => __( "Style 5", 'backstop-themes-admin' ),
							"6" => __( "Style 6", 'backstop-themes-admin' ),
							"7" => __( "Interactive 1", 'backstop-themes-admin' ),
							"8" => __( "Interactive 2", 'backstop-themes-admin' ),
							"9" => __( "Interactive 3", 'backstop-themes-admin' ),
						),
						"type" => "select"
					),
					array(
						"name" => __( "Custom URL", 'backstop-themes-admin' ),
						"id" => "url",
						"desc" => __( 'You can set a custom URL to be displayed with your button instead of the current page.', 'backstop-themes-admin' ),
						"default" => "",
						"type" => "text"
					),
					array(
						"name" => __( "Custom Title", 'backstop-themes-admin' ),
						"id" => "title",
						"desc" => __( 'If using the interactive buttons you can specify a custom title to use here.', 'backstop-themes-admin' ),
						"default" => "",
						"type" => "text"
					),
					array(
						"name" => __( "Styling", 'backstop-themes-admin' ),
						"id" => "disablestyle",
						"desc" => __( 'Checking this will disable the reddit styling used for the button.', 'backstop-themes-admin' ),
						"default" => "",
						"options" => array(
							"true" => __( "Disable reddit styling?", 'backstop-themes-admin' ),
						),
						"type" => "checkbox"
					),
					array(
						"name" => __( "Target", 'backstop-themes-admin' ),
						"id" => "target",
						"desc" => __( 'Select the target for this button.', 'backstop-themes-admin' ),
						"default" => "",
						"options" => array(
							"true" => __( "Display in new window?", 'backstop-themes-admin' ),
						),
						"type" => "checkbox"
					),
					array(
						"name" => __( "Community", 'backstop-themes-admin' ),
						"id" => "community",
						"desc" => __( 'If using the interactive buttons you can specify a community to target here.', 'backstop-themes-admin' ),
						"default" => "",
						"type" => "text"
					),
					"shortcode_has_atts" => true,
				)
			);			
			
			return $option;
		}
		
		extract(shortcode_atts(array(
			'layout'        => '8',
			'url'			=> '',
			'disablestyle'	=> '',
			'target'		=> '',
			'community'		=> '',
			'title'			=> '',
	    	), $atts));
	
		if( is_feed() ) return;
	    	
	    if ($disablestyle != '') { $disablestyle = "&styled=off"; }
	    if ($target != '') { $target = "&newwindow=1"; }
	    if ($layout == '7' || $layout == '8' || $layout == '9') { $url = "reddit_url='".$url."';"; } else { if ($url != '') { $url = "&url='".$url."'"; } }
	    if ($title != '') { $title = "reddit_title='".$title."';"; }
	    if ($community != '') { $community = "reddit_target='".$community."';"; }
	    if ($layout == '7' || $layout == '8' || $layout == '9') { $target = "reddit_newwindow='1';"; }
	    	
		switch ($layout)
		{
			case 1: return '<div class = "mysite_sociable"><script type="text/javascript" src="http://www.reddit.com/buttonlite.js?i=0'.$disablestyle.$url.$target.'"></script></div>'; break;
			case 2: return '<div class = "mysite_sociable"><script type="text/javascript" src="http://www.reddit.com/buttonlite.js?i=1'.$disablestyle.$url.$target.'"></script></div>'; break;		
			case 3: return '<div class = "mysite_sociable"><script type="text/javascript" src="http://www.reddit.com/buttonlite.js?i=2'.$disablestyle.$url.$target.'"></script></div>'; break;		
			case 4: return '<div class = "mysite_sociable"><script type="text/javascript" src="http://www.reddit.com/buttonlite.js?i=3'.$disablestyle.$url.$target.'"></script></div>'; break;		
			case 5: return '<div class = "mysite_sociable"><script type="text/javascript" src="http://www.reddit.com/buttonlite.js?i=4'.$disablestyle.$url.$target.'"></script></div>'; break;		
			case 6: return '<div class = "mysite_sociable"><script type="text/javascript" src="http://www.reddit.com/buttonlite.js?i=5'.$disablestyle.$url.$target.'"></script></div>'; break;	
			case 7: $out = '<div class = "mysite_sociable"><script type="text/javascript" src="http://www.reddit.com/static/button/button1.js"></script>'; 
					$out .= '<script type = "text/javascript">'.$url.$title.$community.$target.'</script></div>';
					return $out; break;
			case 8: $out = '<div class = "mysite_sociable"><script type="text/javascript" src="http://www.reddit.com/static/button/button2.js"></script>';
					$out .= '<script type = "text/javascript">'.$url.$title.$community.$target.'</script></div>';
					return $out; break;
			case 9: $out = '<div class = "mysite_sociable"><script type="text/javascript" src="http://www.reddit.com/static/button/button3.js"></script>'; 
					$out .= '<script type = "text/javascript">'.$url.$title.$community.$target.'</script></div>';
					return $out; break;	
		}
	}
	
	/**
	 *  LinkedIn button
	 */
	function linkedin( $atts = null, $content = null ) {
	
		if( $atts == 'generator' ) {
			$option = array( 
				"name" => __( "LinkedIn", 'backstop-themes-admin' ),
				"value" => "linkedin",
				"options" => array(
					array(
						"name" => __( "Layout", 'backstop-themes-admin' ),
						"id" => "layout",
						"desc" => __( 'Choose how you would like to display the linkedin button.', 'backstop-themes-admin' ),
						"default" => "",
						"options" => array(
							"1" => __( "Style 1", 'backstop-themes-admin' ),
							"2" => __( "Style 2", 'backstop-themes-admin' ),
							"3" => __( "Style 3", 'backstop-themes-admin' ),
						),
						"type" => "select"
					),
					array(
						"name" => __( "Custom URL", 'backstop-themes-admin' ),
						"id" => "url",
						"desc" => __( 'You can set a custom URL to be displayed within linkedin here.', 'backstop-themes-admin' ),
						"default" => "",
						"type" => "text"
					),
					"shortcode_has_atts" => true,
				)
			);			
			
			return $option;
		}
		
		extract(shortcode_atts(array(
			'layout'        => '3',
			'url'			=> '',
	    	), $atts));
	
		if( is_feed() ) return;
	    	
	    if ($url != '') { $url = "data-url='".$url."'"; }
	    if ($layout == '2') { $layout = 'right'; }
		if ($layout == '3') { $layout = 'top'; }
	    	
		return '<div class = "mysite_sociable"><script type="text/javascript" src="http://platform.linkedin.com/in.js"></script><script type="in/share" data-counter = "'.$layout.'" '.$url.'></script></div>';
	}
	
	/**
	 *  Delicious button
	 */
	function delicious( $atts = null, $content = null ) {
	
		if( $atts == 'generator' ) {
			$option = array( 
				"name" => __( "Delicious", 'backstop-themes-admin' ),
				"value" => "delicious",
				"options" => array(
					array(
						"name" => __( "Custom Text", 'backstop-themes-admin' ),
						"id" => "text",
						"desc" => __( 'You can set some text to display alongside your delicious button.', 'backstop-themes-admin' ),
						"default" => "",
						"type" => "text"
					),
					"shortcode_has_atts" => true,
				)
			);			
			
			return $option;
		}
		
		extract(shortcode_atts(array(
			'text'			=> '',
	    ), $atts));
	
		if( is_feed() ) return;
	    	
		return '<div class = "mysite_sociable"><img src="http://www.delicious.com/static/img/delicious.small.gif" height="10" width="10" alt="Delicious" />&nbsp;<a href="http://www.delicious.com/save" onclick="window.open(&#39;http://www.delicious.com/save?v=5&noui&jump=close&url=&#39;+encodeURIComponent(location.href)+&#39;&title=&#39;+encodeURIComponent(document.title), &#39;delicious&#39;,&#39;toolbar=no,width=550,height=550&#39;); return false;">'.$text.'</a></div>';
	}
	
	/**
	 *  Pinterest button
	 */
	function pinterest( $atts = null, $content = null ) {
	
		if( $atts == 'generator' ) {
			$option = array( 
				"name" => __( "Pinterest", 'backstop-themes-admin' ),
				"value" => "pinterest",
				"options" => array(
					array(
						"name" => __( "Description", 'backstop-themes-admin' ),
						"id" => "text",
						"desc" => __( 'You can set some text to display alongside your Pinterest button.', 'backstop-themes-admin' ),
						"default" => "",
						"type" => "text"
					),
					array(
						"name" => __( "Layout", 'backstop-themes-admin' ),
						"id" => "layout",
						"desc" => __( 'Choose how you would like to display the Pinterest button.', 'backstop-themes-admin' ),
						"default" => "",
						"options" => array(
							"horizontal" => __( "Horizontal", 'backstop-themes-admin' ),
							"vertical" => __( "Vertical", 'backstop-themes-admin' ),
							"none" => __( "None", 'backstop-themes-admin' ),
						),
						"type" => "select"
					),
					array(
						"name" => __( "Image URL", 'backstop-themes-admin' ),
						"id" => "image",
						"desc" => __( 'Paste the URL of the image you want to be pinned here.', 'backstop-themes-admin' ),
						"default" => "",
						"type" => "text",
					),
					array(
						"name" => __( "Auto Prompt", 'backstop-themes-admin' ),
						"id" => "prompt",
						"desc" => __( 'Check this if you wish to have a prompt display to select your image when clicking on the Pinterest button. This will disable the count bubble.', 'backstop-themes-admin' ),
						"options" => array( "true" => __( "Use Auto Prompt", 'backstop-themes-admin' ) ),  
						"default" => "",
						"type" => "checkbox",
					),
					"shortcode_has_atts" => true,
				)
			);			
			
			return $option;
		}
		
		extract(shortcode_atts(array(
			'text'			=> '',
			'layout'		=> '',
			'image'			=> '',
			'url'			=> '',
			'prompt'	=> '',
	    ), $atts));
	
		if( is_feed() ) return;
	    	
		if ($url == '') { $url = get_permalink(); }
		if ($layout == '') { $layout = 'horizontal'; }
			
		$out = '<div class = "mysite_sociable"><a href="http://pinterest.com/pin/create/button/?url='.$url.'&media='.$image.'&description='.$text.'" class="pin-it-button" count-layout="'.$layout.'">Pin It</a>';
		$out .= '<script type="text/javascript" src="http://assets.pinterest.com/js/pinit.js"></script></div>';
		
		if ( $prompt ) {
			$out = '<div class = "mysite_sociable"><a title="Pin It on Pinterest" class="pin-it-button" href="javascript:void(0)">pin it</a>';
			$out .= '<script type = "text/javascript">';
			$out .= 'jQuery(document).ready(function(){';
				$out .= 'jQuery(".pin-it-button").click(function(event) {';
				$out .= 'event.preventDefault();';
				$out .= 'jQuery.getScript("http://assets.pinterest.com/js/pinmarklet.js?r=" + Math.random()*99999999);';
				$out .= '});';
			$out .= '});';
			$out .= '</script>';
			$out .= '<style type = "text/css">a.pin-it-button {position: absolute;background: url(http://assets.pinterest.com/images/pinit6.png);font: 11px Arial, sans-serif;text-indent: -9999em;font-size: .01em;color: #CD1F1F;height: 20px;width: 43px;background-position: 0 -7px;}a.pin-it-button:hover {background-position: 0 -28px;}a.pin-it-button:active {background-position: 0 -49px;}</style></div>';
		}
			
		return $out;
	}
	
	/**
	 *
	 */
	function _options( $class ) {
		$shortcode = array();
		
		$class_methods = get_class_methods( $class );
		
		foreach( $class_methods as $method ) {
			if( $method[0] != '_' )
				$shortcode[] = call_user_func(array( &$class, $method ), $atts = 'generator' );
		}
		
		$options = array(
			"name" => __( 'Social', 'backstop-themes-admin' ),
			'desc' => __( 'Choose which type of social button you wish to use.', 'backstop-themes-admin' ),
			"value" => "social",
			"options" => $shortcode,
			"shortcode_has_types" => true
		);
		
		return $options;
	}
}

?>